//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';
import 'package:proximity/screens/home_screen/home_screen.dart';
import 'package:proximity/screens/no_internet_screen/no_internet_screen.dart';
import 'package:proximity/screens/registration_screen/registration_screen.dart';
import 'package:proximity/stores/auth_store.dart';
import 'package:proximity/utils/connectivity_provider.dart';

final _authStore = AuthStore();

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isOnline = Provider.of<ConnectivityProvider>(context).isOnline;

    if (!isOnline) {
      if (Navigator.canPop(context)) {
        Navigator.pop(context);
      }
      return const NoInternetScreen();
    } else {
      return Observer(builder: (context) {
        if (_authStore.isLoggedIn()) {
          return const HomeScreen();
        } else {
          return const RegistrationScreen();
        }
      });
    }
  }
}
