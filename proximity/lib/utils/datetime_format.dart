//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

String _appendLeadingZero(int value) {
  if (value < 10) {
    return "0$value";
  }
  return "$value";
}

String formatDate(DateTime date) {
  return " ${_appendLeadingZero(date.year)}"
      "-${_appendLeadingZero(date.month)}"
      "-${_appendLeadingZero(date.day)}";
}

String formatTime(DateTime date) {
  return " ${_appendLeadingZero(date.hour)}"
      ":${_appendLeadingZero(date.minute)}";
}
