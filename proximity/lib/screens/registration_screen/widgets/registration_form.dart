//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:proximity/screens/registration_screen/widgets/register_button.dart';

import '../../../stores/auth_store.dart';
import '../../../stores/registration_store.dart';
import 'close_form_button.dart';
import 'form_inputs.dart';
import 'login_button.dart';

final _authStore = AuthStore();
final _registrationStore = RegistrationStore();

class RegistrationForm extends StatelessWidget {
  const RegistrationForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return Expanded(
      child: Column(
        children: [
          Padding(
            padding:
                EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.15),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                Observer(builder: (_) {
                  if (_registrationStore.activeForm == ActiveForm.register) {
                    return Padding(
                      padding: const EdgeInsets.all(10),
                      child: usernameInput(),
                    );
                  }
                  return Container();
                }),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: emailInput(),
                ),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: passwordInput(),
                ),
                Observer(builder: (_) {
                  if (_registrationStore.activeForm == ActiveForm.register) {
                    return Padding(
                      padding: const EdgeInsets.all(10),
                      child: passwordRepeatInput(),
                    );
                  }
                  return Container();
                }),
              ],
            ),
          ),
          const Spacer(),
          Observer(builder: (_) {
            if (_authStore.isLoading) {
              return const CircularProgressIndicator();
            } else {
              if (_registrationStore.activeForm == ActiveForm.register) {
                return const RegisterButton();
              }
              return const LoginButton();
            }
          }),
          Observer(builder: (_) {
            if (_authStore.isLoading) {
              return Container();
            }
            return const CloseFormButton();
          }),
        ],
      ),
    );
  }
}
