//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../../stores/registration_store.dart';
import 'form_text_input.dart';

final _registrationStore = RegistrationStore();

Widget passwordInput() {
  return Observer(
    builder: (_) => FormTextInput(
        obscureText: true,
        initialValue: _registrationStore.password,
        icon: Icon(Icons.lock,
            color: _registrationStore.passwordError == null
                ? Colors.white
                : Colors.red),
        labelText: "Password",
        hintText: "Enter your password.",
        errorText: _registrationStore.passwordError,
        onChanged: (value) => _registrationStore.setPassword(value)),
  );
}

Widget passwordRepeatInput() {
  return Observer(
    builder: (_) => FormTextInput(
        obscureText: true,
        initialValue: _registrationStore.passwordRepeat,
        icon: Icon(Icons.lock,
            color: _registrationStore.passwordRepeatError == null
                ? Colors.white
                : Colors.red),
        labelText: "Repeat password",
        hintText: "Enter your password again.",
        errorText: _registrationStore.passwordRepeatError,
        onChanged: (value) => _registrationStore.setPasswordRepeat(value)),
  );
}

Widget emailInput() {
  return Observer(
    builder: (_) => FormTextInput(
        initialValue: _registrationStore.email,
        icon: Icon(Icons.email,
            color: _registrationStore.emailError == null
                ? Colors.white
                : Colors.red),
        labelText: "E-Mail",
        hintText: "Enter your e-mail.",
        errorText: _registrationStore.emailError,
        onChanged: (value) => _registrationStore.setEmail(value)),
  );
}

Widget usernameInput() {
  return Observer(
    builder: (_) => FormTextInput(
        initialValue: _registrationStore.username,
        icon: Icon(Icons.person,
            color: _registrationStore.usernameError == null
                ? Colors.white
                : Colors.red),
        labelText: "Username",
        hintText: "Enter your username.",
        errorText: _registrationStore.usernameError,
        onChanged: (value) => _registrationStore.setUsername(value)),
  );
}
