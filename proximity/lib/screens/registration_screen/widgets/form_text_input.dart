//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';

import '../../../utils/open_sans_font.dart';

class FormTextInput extends StatelessWidget with OpenSansFont {
  final String? initialValue;
  final String? labelText;
  final String? hintText;
  final String? errorText;
  final bool obscureText;
  final Icon icon;
  final void Function(String?) onChanged;
  final Color? color;
  final TextInputType? type;

  const FormTextInput({
    Key? key,
    this.obscureText = false,
    required this.labelText,
    required this.hintText,
    required this.errorText,
    required this.icon,
    required this.onChanged,
    this.initialValue,
    this.color,
    this.type,
  }) : super(key: key);

  OutlineInputBorder _errorBorderDecoration() => OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: const BorderSide(color: Colors.red, width: 2.5),
      );

  OutlineInputBorder _borderDecoration() => OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: BorderSide(color: color ?? Colors.white, width: 2.5),
      );

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: type,
      cursorColor: color ?? Colors.white,
      obscureText: obscureText,
      initialValue: initialValue,
      onChanged: onChanged,
      style: openSansFont(
        fontSize: 13.5,
        color: color ?? Colors.white,
        decorationColor: color ?? Colors.white,
      ),
      decoration: InputDecoration(
        floatingLabelStyle: TextStyle(
          color: color ?? Colors.white,
          decorationColor: color ?? Colors.white,
        ),
        icon: icon,
        border: _borderDecoration(),
        enabledBorder: _borderDecoration(),
        focusedBorder: _borderDecoration(),
        errorBorder: _errorBorderDecoration(),
        labelText: labelText,
        hintText: hintText,
        errorText: errorText,
        errorStyle: const TextStyle(color: Colors.red, fontSize: 15),
        hintStyle: openSansFont(
          fontSize: 13.5,
          color: color ?? Colors.white,
          decorationColor: color ?? Colors.white,
        ),
        labelStyle: openSansFont(
          fontSize: 13.5,
          color: color ?? Colors.white,
          decorationColor: color ?? Colors.white,
        ),
        focusColor: color ?? Colors.white,
        fillColor: color ?? Colors.white,
      ),
    );
  }
}
