//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';

import '../../../stores/auth_store.dart';
import '../../../stores/registration_store.dart';
import '../../../utils/open_sans_font.dart';

final _authStore = AuthStore();
final _registrationStore = RegistrationStore();

class LoginButton extends StatelessWidget with OpenSansFont {
  const LoginButton({Key? key}) : super(key: key);

  Future<void> _login() async {
    _registrationStore.validateLogin();
    if (_registrationStore.hasLoginErrors) {
      return;
    } else if (_registrationStore.isLoginValid) {
      await _authStore.login(
        _registrationStore.email!,
        _registrationStore.password!,
      );

      _registrationStore.resetForm();
      _registrationStore.setActiveForm(ActiveForm.none);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: _login,
      style: ElevatedButton.styleFrom(
        backgroundColor: Colors.pinkAccent,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
        minimumSize: Size(MediaQuery.of(context).size.width * 0.85, 50),
      ),
      child: Text(
        "Login",
        style: openSansFont(fontWeight: FontWeight.w600),
      ),
    );
  }
}
