//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';

import '../../../stores/auth_store.dart';
import '../../../stores/registration_store.dart';
import '../../../utils/open_sans_font.dart';

final _authStore = AuthStore();
final _registrationStore = RegistrationStore();

class RegisterButton extends StatelessWidget with OpenSansFont {
  const RegisterButton({Key? key}) : super(key: key);

  Future<void> _register() async {
    _registrationStore.validateRegister();
    if (_registrationStore.hasRegistrationErrors) {
      return;
    } else if (_registrationStore.isRegistrationValid) {
      await _authStore.register(_registrationStore.username!,
          _registrationStore.email!, _registrationStore.password!);

      _registrationStore.resetForm();
      _registrationStore.setActiveForm(ActiveForm.none);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: _register,
      style: ElevatedButton.styleFrom(
        backgroundColor: Colors.amberAccent,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
        minimumSize: Size(MediaQuery.of(context).size.width * 0.85, 50),
      ),
      child: Text(
        "Register",
        style: openSansFont(
          color: Colors.black,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }
}
