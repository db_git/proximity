//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:proximity/screens/registration_screen/widgets/app_title.dart';
import 'package:proximity/screens/registration_screen/widgets/guest_login_button.dart';
import 'package:proximity/screens/registration_screen/widgets/registration_form.dart';
import 'package:proximity/screens/registration_screen/widgets/show_login_form_button.dart';
import 'package:proximity/screens/registration_screen/widgets/show_register_form_button.dart';

import '../../stores/registration_store.dart';

final _registrationStore = RegistrationStore();

class RegistrationScreen extends StatelessWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  Decoration _containerDecoration() {
    return BoxDecoration(
      image: DecorationImage(
        image: const AssetImage("assets/login_background.webp"),
        fit: BoxFit.cover,
        colorFilter:
        ColorFilter.mode(Colors.black.withOpacity(0.65), BlendMode.darken),
      ),
    );
  }

  EdgeInsets _containerMargin(BuildContext context) {
    return EdgeInsets.only(
        top: MediaQuery
            .of(context)
            .size
            .height * 0.05,
        bottom: MediaQuery
            .of(context)
            .size
            .height * 0.015);
  }

  List<Widget> _registrationForm() {
    switch (_registrationStore.activeForm) {
      case ActiveForm.login:
      case ActiveForm.register:
        {
          return const [
            RegistrationForm(),
          ];
        }
      case ActiveForm.none:
        {
          return <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 50),
              child: Text(
                "Discover local events around you.",
                style: GoogleFonts.comfortaa(
                  textStyle:
                  const TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
          ] +
              const [
                Spacer(),
                Padding(
                  padding: EdgeInsets.only(bottom: 10),
                  child: ShowLoginFormButton(),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 10),
                  child: ShowRegisterFormButton(),
                ),
                GuestLoginButton(),
              ];
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Center(
        child: Container(
          constraints: const BoxConstraints.expand(),
          decoration: _containerDecoration(),
          child: Container(
            margin: _containerMargin(context),
            child: Observer(
              builder: (_) =>
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const AppTitle(),
                      ..._registrationForm(),
                    ],
                  ),
            ),
          ),
        ),
      ),
    );
  }
}
