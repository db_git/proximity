//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:proximity/screens/home_screen/widgets/appbar_avatar.dart';
import 'package:proximity/screens/home_screen/widgets/create_event.dart';
import 'package:proximity/screens/home_screen/widgets/event_list.dart';
import 'package:proximity/screens/home_screen/widgets/search_button.dart';
import 'package:proximity/utils/open_sans_font.dart';

class HomeScreen extends StatelessWidget with OpenSansFont {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Discover events",
          style: openSansFont(
            fontWeight: FontWeight.w700,
            fontSize: 18,
          ),
        ),
        backgroundColor: Colors.indigoAccent,
        actions: const [
          CreateEvent(),
          SearchButton(),
          AppBarAvatar(),
        ],
      ),
      body: const EventList(),
    );
  }
}
