//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:proximity/models/event.dart';
import 'package:proximity/screens/home_screen/widgets/event_card_properties.dart';
import 'package:proximity/utils/datetime_format.dart';
import 'package:proximity/utils/open_sans_font.dart';

class EventCardFirstRow extends StatelessWidget
    with OpenSansFont, EventCardProperties {
  final Event event;

  const EventCardFirstRow({Key? key, required this.event}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(Icons.calendar_month, size: iconSize, color: Colors.blueGrey),
        Text(
          formatDate(event.date),
          style: openSansFont(fontSize: iconTextSize, color: Colors.blueGrey),
        ),
        const Padding(padding: EdgeInsets.only(left: 5, right: 5)),
        Icon(Icons.access_time, size: iconSize, color: Colors.blueGrey),
        Text(
          formatTime(event.date),
          style: openSansFont(fontSize: iconTextSize, color: Colors.blueGrey),
        ),
      ],
    );
  }
}
