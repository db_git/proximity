//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:proximity/models/event.dart';
import 'package:proximity/screens/event_screen/event_screen.dart';
import 'package:proximity/screens/home_screen/widgets/event_card_body.dart';
import 'package:proximity/screens/home_screen/widgets/event_card_first_row.dart';
import 'package:proximity/screens/home_screen/widgets/event_card_second_row.dart';
import 'package:proximity/screens/home_screen/widgets/event_card_third_row.dart';

class EventCard extends StatelessWidget {
  final Event event;

  const EventCard({Key? key, required this.event}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return EventCardBody(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute<void>(
            builder: (BuildContext context) {
              return EventScreen(
                eventId: event.id,
              );
            },
          ),
        );
      },
      children: [
        EventCardFirstRow(event: event),
        EventCardSecondRow(event: event),
        EventCardThirdRow(event: event),
      ],
    );
  }
}
