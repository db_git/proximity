//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:proximity/screens/profile_screen/profile_screen.dart';
import 'package:proximity/stores/auth_store.dart';
import 'package:proximity/stores/profile_store.dart';

final _authStore = AuthStore();
final _profileStore = ProfileStore();

class AppBarAvatar extends StatelessWidget {
  const AppBarAvatar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final userId = _authStore.getId();
    if (userId != null) _profileStore.getById(userId);

    return Padding(
      padding: const EdgeInsets.only(right: 15),
      child: IconButton(
        icon: ClipRRect(
          borderRadius: BorderRadius.circular(50),
          child: Observer(
            builder: (_) => _authStore.isGuest() ||
                    _profileStore.user == null ||
                    _profileStore.user!.avatarUrl == null
                ? Image.asset("assets/avatar_placeholder.webp")
                : CachedNetworkImage(
                    imageUrl: _profileStore.user!.avatarUrl!,
                    placeholder: (context, url) =>
                        const CircularProgressIndicator(),
                  ),
          ),
        ),
        tooltip: "Profile",
        onPressed: () {
          if (_authStore.isGuest()) {
            _authStore.logout();
            return;
          }

          Navigator.push(
            context,
            MaterialPageRoute<void>(
              builder: (BuildContext context) {
                return ProfileScreen(user: _profileStore.user!);
              },
            ),
          );
        },
      ),
    );
  }
}
