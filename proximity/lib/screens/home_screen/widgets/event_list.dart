//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:proximity/models/event.dart';
import 'package:proximity/screens/home_screen/widgets/event_card.dart';
import 'package:proximity/stores/event_store.dart';
import 'package:proximity/utils/open_sans_font.dart';

final _eventStore = EventStore();

class EventList extends StatefulWidget {
  const EventList({super.key});

  @override
  State<EventList> createState() => _EventListState();
}

class _EventListState extends State<EventList> with OpenSansFont {
  bool _isFirstLoading = false;
  bool _isLoadingMore = false;

  late final ScrollController _controller;
  final List<Event> _events = <Event>[];

  Future<void> _firstLoad() async {
    setState(() => _isFirstLoading = true);

    _eventStore.setPage(1);
    await _eventStore.get();

    setState(
      () {
        _events.clear();
        _events.addAll(_eventStore.events);
      },
    );

    setState(() => _isFirstLoading = false);
  }

  Future<void> _loadMore() async {
    if (!_eventStore.hasNextPage ||
        _isFirstLoading ||
        _isLoadingMore ||
        _controller.position.extentAfter >= 300) {
      return;
    }

    setState(() => _isLoadingMore = true);

    _eventStore.setPage(_eventStore.page + 1);
    await _eventStore.get();

    setState(() => _events.addAll(_eventStore.events));
    setState(() => _isLoadingMore = false);
  }

  @override
  void initState() {
    super.initState();

    _firstLoad();
    _controller = ScrollController();
    _controller.addListener(_loadMore);
  }

  @override
  void dispose() {
    _controller.removeListener(_loadMore);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Future<void> onRefresh() async {
      await _firstLoad();
    }

    if (_isFirstLoading) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    }

    if (_events.isEmpty) {
      return RefreshIndicator(
        onRefresh: onRefresh,
        child: LayoutBuilder(
          builder: (context, constraints) => ListView(
            children: [
              Container(
                padding: const EdgeInsets.all(20),
                constraints: BoxConstraints(
                  minHeight: constraints.maxHeight,
                ),
                child: Center(
                  child: Text(
                    "No events found...",
                    style: openSansFont(
                      color: Colors.black,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }

    return RefreshIndicator(
      onRefresh: onRefresh,
      child: Column(
        children: [
          Expanded(
            child: ListView.builder(
              physics: const AlwaysScrollableScrollPhysics(),
              controller: _controller,
              itemCount: _events.length,
              itemBuilder: (context, index) => Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: EventCard(
                  event: _events[index],
                ),
              ),
            ),
          ),
          if (_isLoadingMore)
            const Padding(
              padding: EdgeInsets.only(top: 40, bottom: 40),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
        ],
      ),
    );
  }
}
