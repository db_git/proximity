//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:proximity/models/event.dart';
import 'package:proximity/screens/home_screen/widgets/event_card_properties.dart';
import 'package:proximity/utils/open_sans_font.dart';

class EventCardSecondRow extends StatelessWidget
    with EventCardProperties, OpenSansFont {
  final Event event;

  const EventCardSecondRow({Key? key, required this.event}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> eventSubtitle = [Container()];

    final double sizedBoxTextWidth =
        MediaQuery.of(context).size.width * textPercentage -
            paddingAll -
            paddingLeft -
            paddingRight;

    final double sizedBoxImageWidth =
        MediaQuery.of(context).size.width * imagePercentage -
            paddingAll -
            paddingLeft -
            paddingRight;

    if (event.subtitle != null) {
      eventSubtitle = [
        Padding(padding: EdgeInsets.all(paddingAll)),
        SizedBox(
          width: sizedBoxTextWidth,
          child: Text(
            event.subtitle!,
            textAlign: TextAlign.left,
            style: openSansFont(
              color: Colors.grey.shade700,
              fontSize: subtitleFontSize,
            ),
          ),
        )
      ];
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          children: [
            Padding(padding: EdgeInsets.all(paddingAll)),
            SizedBox(
              width: sizedBoxTextWidth,
              child: Text(
                event.title,
                textAlign: TextAlign.left,
                style: openSansFont(
                  color: Colors.black,
                  fontSize: titleFontSize,
                ),
              ),
            ),
            ...eventSubtitle,
            Padding(padding: EdgeInsets.all(paddingAll)),
          ],
        ),
        SizedBox(
          width: sizedBoxImageWidth,
          child: ClipRRect(
            borderRadius: borderRadius,
            child: FadeInImage(
              image: NetworkImage(event.pictureUrl),
              placeholder: const AssetImage("assets/event_placeholder.webp"),
            ),
          ),
        ),
      ],
    );
  }
}
