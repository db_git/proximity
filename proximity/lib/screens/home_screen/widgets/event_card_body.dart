//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:proximity/screens/home_screen/widgets/event_card_properties.dart';

class EventCardBody extends StatelessWidget with EventCardProperties {
  final List<Widget> children;
  final void Function() onTap;

  const EventCardBody({
    Key? key,
    required this.children,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(paddingLeft, 10, paddingRight, 5),
      child: Center(
        child: Card(
          elevation: 8,
          shape: RoundedRectangleBorder(
            side: BorderSide(
              color: Theme.of(context).colorScheme.outline,
            ),
            borderRadius: borderRadius,
          ),
          child: InkWell(
            borderRadius: borderRadius,
            splashColor: Colors.blue.withAlpha(30),
            onTap: onTap,
            child: Padding(
              padding: EdgeInsets.all(paddingAll),
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: children,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
