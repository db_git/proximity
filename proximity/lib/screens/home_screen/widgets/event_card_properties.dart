//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';

abstract class EventCardProperties {
  double get paddingAll => 12;

  double get paddingLeft => 15;

  double get paddingRight => 15;

  double get textPercentage => 0.6;

  double get imagePercentage => 1 - textPercentage;

  double get iconSize => 20;

  double get iconTextSize => 14;

  double get titleFontSize => 24;

  double get subtitleFontSize => 16;

  BorderRadius get borderRadius => const BorderRadius.all(Radius.circular(18));
}
