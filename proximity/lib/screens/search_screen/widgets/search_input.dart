//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:proximity/stores/event_search_store.dart';

final _eventSearchStore = EventSearchStore();

class SearchInput extends StatelessWidget {
  const SearchInput({super.key});

  @override
  Widget build(BuildContext context) {
    final TextEditingController textController = TextEditingController(
      text: _eventSearchStore.search,
    );

    return TextField(
      controller: textController,
      onSubmitted: (_) => _eventSearchStore.searchEvents(),
      onChanged: (value) => _eventSearchStore.setSearch(value),
      decoration: InputDecoration(
        prefixIcon: const Icon(Icons.search),
        prefixIconColor: Colors.indigoAccent,
        suffixIconColor: Colors.indigoAccent,
        suffixIcon: IconButton(
          icon: const Icon(Icons.clear),
          onPressed: () {
            textController.clear();
            _eventSearchStore.setSearch("");
          },
        ),
        hintText: 'Search...',
        border: InputBorder.none,
      ),
    );
  }
}
