//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:proximity/screens/home_screen/widgets/event_card.dart';
import 'package:proximity/stores/event_search_store.dart';

final _eventSearchStore = EventSearchStore();

class SearchBody extends StatelessWidget {
  const SearchBody({super.key});

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (context) {
        if (_eventSearchStore.isLoading) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else if (_eventSearchStore.events.isEmpty) {
          return const Center(
            child: Text("Enter keyword to search..."),
          );
        } else {
          return ListView.builder(
            itemCount: _eventSearchStore.events.length,
            itemBuilder: (context, index) =>
                EventCard(event: _eventSearchStore.events[index]),
          );
        }
      },
    );
  }
}
