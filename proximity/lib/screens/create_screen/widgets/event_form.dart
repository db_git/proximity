//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:proximity/screens/create_screen/widgets/form_inputs.dart';
import 'package:proximity/screens/home_screen/home_screen.dart';
import 'package:proximity/stores/create_event_store.dart';
import 'package:proximity/utils/open_sans_font.dart';

final _createEventStore = CreateEventStore();

class EventForm extends StatelessWidget with OpenSansFont {
  const EventForm({super.key});

  static const padding = EdgeInsets.all(15);

  @override
  Widget build(BuildContext context) {
    Future<void> onPressed() async {
      _createEventStore.validate();

      if (_createEventStore.isValid) {
        await _createEventStore.create();

        if (!context.mounted) return;

        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            duration: Duration(milliseconds: 1750),
            content: Text("Event created."),
          ),
        );

        Navigator.of(context).pop();
        Navigator.of(context).pop();
        Navigator.push(
          context,
          MaterialPageRoute<void>(
            builder: (BuildContext context) {
              return const HomeScreen();
            },
          ),
        );
      }
    }

    return ListView(
      children: [
        Padding(
          padding: padding,
          child: titleInput(),
        ),
        Padding(
          padding: padding,
          child: subtitleInput(),
        ),
        Padding(
          padding: padding,
          child: dateInput(),
        ),
        Padding(
          padding: padding,
          child: pictureInput(),
        ),
        Padding(
          padding: padding,
          child: locationInput(),
        ),
        Padding(
          padding: padding,
          child: descriptionInput(),
        ),
        Padding(
          padding: padding,
          child: attendeeLimitInput(),
        ),
        Padding(
          padding: padding,
          child: priceInput(),
        ),
        Padding(
          padding: const EdgeInsets.all(30),
          child: ElevatedButton(
            onPressed: onPressed,
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.deepOrangeAccent,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(35)),
              minimumSize: Size(MediaQuery.of(context).size.width * 0.5, 50),
            ),
            child: Text(
              "Create",
              style: openSansFont(
                color: Colors.black,
                fontSize: 14,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
