//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:proximity/screens/registration_screen/widgets/form_text_input.dart';
import 'package:proximity/stores/create_event_store.dart';

final _createEventStore = CreateEventStore();

Widget titleInput() {
  return Observer(
    builder: (context) => FormTextInput(
      color: Colors.black,
      initialValue: _createEventStore.title,
      labelText: "Title",
      hintText: "Event title",
      errorText: _createEventStore.titleError,
      icon: Icon(
        Icons.title,
        color: _createEventStore.titleError == null ? Colors.black : Colors.red,
      ),
      onChanged: (value) => _createEventStore.setTitle(value),
    ),
  );
}

Widget subtitleInput() {
  return Observer(
    builder: (context) => FormTextInput(
      color: Colors.black,
      initialValue: _createEventStore.subtitle,
      labelText: "Subtitle",
      hintText: "Optional info",
      errorText: _createEventStore.subtitleError,
      icon: Icon(
        Icons.subtitles,
        color:
            _createEventStore.subtitleError == null ? Colors.black : Colors.red,
      ),
      onChanged: (value) => _createEventStore.setSubtitle(value),
    ),
  );
}

Widget dateInput() {
  final OutlineInputBorder errorBorderDecoration = OutlineInputBorder(
    borderRadius: BorderRadius.circular(12),
    borderSide: const BorderSide(color: Colors.red, width: 2.5),
  );

  final OutlineInputBorder borderDecoration = OutlineInputBorder(
    borderRadius: BorderRadius.circular(12),
    borderSide: const BorderSide(color: Colors.black, width: 2.5),
  );

  final TextStyle style = GoogleFonts.openSans(
    textStyle: const TextStyle(
      color: Colors.black,
      fontSize: 13.5,
      decorationColor: Colors.black,
    ),
  );

  final TextStyle errorStyle = GoogleFonts.openSans(
    textStyle: const TextStyle(
      color: Colors.red,
      fontSize: 15,
      fontWeight: FontWeight.w500,
      decorationColor: Colors.red,
    ),
  );

  return Observer(
    builder: (_) => DateTimePicker(
      type: DateTimePickerType.dateTime,
      initialValue: _createEventStore.date?.toString(),
      firstDate: DateTime.now().add(
        const Duration(hours: 12),
      ),
      lastDate: DateTime.now().add(
        const Duration(days: 3650),
      ),
      autovalidate: false,
      onChanged: (value) => _createEventStore.setDate(DateTime.parse(value)),
      decoration: InputDecoration(
        icon: Icon(
          Icons.calendar_month,
          color:
              _createEventStore.dateError == null ? Colors.black : Colors.red,
        ),
        border: borderDecoration,
        enabledBorder: borderDecoration,
        focusedBorder: borderDecoration,
        errorBorder: errorBorderDecoration,
        focusedErrorBorder: errorBorderDecoration,
        errorText: _createEventStore.dateError,
        errorStyle: errorStyle,
        labelText: "Date",
        labelStyle: style,
        hintText: "Date and time of attendace",
        hintStyle: style,
      ),
    ),
  );
}

Widget pictureInput() {
  return Observer(
    builder: (context) => FormTextInput(
      color: Colors.black,
      initialValue: _createEventStore.pictureUrl,
      labelText: "Picture",
      hintText: "Picture URL",
      errorText: _createEventStore.pictureUrlError,
      icon: Icon(
        Icons.image,
        color: _createEventStore.pictureUrlError == null
            ? Colors.black
            : Colors.red,
      ),
      onChanged: (value) => _createEventStore.setPictureUrl(value),
    ),
  );
}

Widget locationInput() {
  return Observer(
    builder: (context) => FormTextInput(
      color: Colors.black,
      initialValue: _createEventStore.location,
      labelText: "Location",
      hintText: "Address of event",
      errorText: _createEventStore.locationError,
      icon: Icon(
        Icons.location_on,
        color:
            _createEventStore.locationError == null ? Colors.black : Colors.red,
      ),
      onChanged: (value) => _createEventStore.setLocation(value),
    ),
  );
}

Widget descriptionInput() {
  return Observer(
    builder: (context) => FormTextInput(
      color: Colors.black,
      initialValue: _createEventStore.description,
      labelText: "Description",
      hintText: "Event information",
      errorText: _createEventStore.descriptionError,
      icon: Icon(
        Icons.text_fields,
        color: _createEventStore.descriptionError == null
            ? Colors.black
            : Colors.red,
      ),
      onChanged: (value) => _createEventStore.setDescription(value),
    ),
  );
}

Widget attendeeLimitInput() {
  return Observer(
    builder: (context) => Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        FormTextInput(
          type: TextInputType.number,
          color: Colors.black,
          initialValue: _createEventStore.attendeeLimit?.toString(),
          labelText: "Attendee limit",
          hintText: "Event title",
          errorText: _createEventStore.attendeeLimitError,
          icon: Icon(
            Icons.people,
            color: _createEventStore.attendeeLimitError == null
                ? Colors.black
                : Colors.red,
          ),
          onChanged: (value) =>
              _createEventStore.setLimt(int.tryParse(value ?? "", radix: 10)),
        ),
        const Padding(
          padding: EdgeInsets.only(
            top: 10,
            left: 50,
          ),
          child: Text(
            "\u2022 Set 0 to have no limit on attendees.",
          ),
        ),
      ],
    ),
  );
}

Widget priceInput() {
  return Observer(
    builder: (context) => Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        FormTextInput(
          type: TextInputType.number,
          color: Colors.black,
          initialValue: _createEventStore.price?.toString(),
          labelText: "Price",
          hintText: "Event price",
          errorText: _createEventStore.priceError,
          icon: Icon(
            Icons.euro,
            color: _createEventStore.priceError == null
                ? Colors.black
                : Colors.red,
          ),
          onChanged: (value) =>
              _createEventStore.setPrice(int.tryParse(value ?? "", radix: 10)),
        ),
        const Padding(
          padding: EdgeInsets.only(
            top: 10,
            left: 50,
          ),
          child: Text(
            "\u2022 Set 0 to make event free to attend.",
          ),
        ),
      ],
    ),
  );
}
