//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:proximity/models/event_details.dart';
import 'package:proximity/screens/event_screen/widgets/event_info_icon.dart';
import 'package:proximity/utils/datetime_format.dart';
import 'package:proximity/utils/open_sans_font.dart';

class EventInfo extends StatelessWidget with OpenSansFont {
  final EventDetails event;

  const EventInfo({Key? key, required this.event}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var rowPadding =
        const Padding(padding: EdgeInsets.only(top: 10, bottom: 10));

    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
      child: Wrap(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  EventInfoIcon(
                    data: "@${event.organizer.username}",
                    icon: Icons.person,
                  ),
                  rowPadding,
                  EventInfoIcon(
                    data: formatDate(event.date),
                    icon: Icons.calendar_month,
                  ),
                  rowPadding,
                  EventInfoIcon(
                    data: formatTime(event.date),
                    icon: Icons.access_time,
                  ),
                  rowPadding,
                  EventInfoIcon(
                    data: " ${event.location}",
                    icon: Icons.location_on_outlined,
                  ),
                  rowPadding,
                  EventInfoIcon(
                    data:
                        " ${event.attendees.length} ${event.attendeeLimit == 0 ? "/ No limit" : "/ ${event.attendeeLimit}"}",
                    icon: Icons.people_alt_outlined,
                  ),
                  rowPadding,
                  EventInfoIcon(
                    data: event.price == 0
                        ? "Event is free to attend."
                        : "${event.price} EUR",
                    icon: Icons.attach_money,
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
