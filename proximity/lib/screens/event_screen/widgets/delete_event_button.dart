//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:proximity/screens/home_screen/home_screen.dart';
import 'package:proximity/stores/delete_event_store.dart';
import 'package:proximity/stores/event_store.dart';

final _eventStore = EventStore();
final _deleteEventStore = DeleteEventStore();

class DeleteEventButton extends StatelessWidget {
  final String eventId;

  const DeleteEventButton({super.key, required this.eventId});

  @override
  Widget build(BuildContext context) {
    void onPressed() {
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) => Observer(
          builder: (context) => AlertDialog(
            title: const Text("Are you sure you want to delete this event?"),
            actions: [
              TextButton(
                onPressed: _deleteEventStore.isLoading
                    ? null
                    : () => Navigator.pop(context, 'Cancel'),
                child: const Text('Cancel'),
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.red,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                ),
                onPressed: _deleteEventStore.isLoading
                    ? null
                    : () async {
                        await _deleteEventStore.delete(eventId);
                        _eventStore.setEvent(null);

                        if (!context.mounted) return;

                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            duration: Duration(milliseconds: 2500),
                            content: Text("Event deleted."),
                          ),
                        );

                        Navigator.of(context).pop('Delete');
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                        Navigator.push(
                          context,
                          MaterialPageRoute<void>(
                            builder: (BuildContext context) {
                              return const HomeScreen();
                            },
                          ),
                        );
                      },
                child: const Text('Delete'),
              )
            ],
          ),
        ),
      );
    }

    return IconButton(
      tooltip: "Delete",
      onPressed: onPressed,
      icon: const Icon(Icons.delete_forever),
      color: Colors.redAccent,
    );
  }
}
