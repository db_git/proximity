//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:proximity/models/event_details.dart';
import 'package:proximity/stores/event_store.dart';
import 'package:proximity/utils/open_sans_font.dart';

final _eventStore = EventStore();

class EventCancelAttendButton extends StatelessWidget with OpenSansFont {
  final EventDetails event;

  const EventCancelAttendButton({Key? key, required this.event})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    void cancelAttendance() {
      _eventStore.cancelAttendEvent(event.id).then(
        (value) {
          if (!context.mounted) return;

          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              duration: Duration(milliseconds: 2500),
              content: Text("Attendance canceled."),
            ),
          );

          _eventStore.getById(event.id);
        },
      );
    }

    return Padding(
      padding: const EdgeInsets.all(35),
      child: ElevatedButton(
        onPressed: cancelAttendance,
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.red,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          minimumSize: Size(MediaQuery.of(context).size.width * 0.5, 50),
        ),
        child: Text(
          "Cancel attendance",
          style: openSansFont(
            fontSize: 18,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }
}
