//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:proximity/models/event_details.dart';
import 'package:proximity/screens/event_screen/widgets/event_attend_button.dart';
import 'package:proximity/screens/event_screen/widgets/event_cancel_attend_button.dart';
import 'package:proximity/screens/event_screen/widgets/event_disabled_attend_button.dart';
import 'package:proximity/stores/auth_store.dart';
import 'package:proximity/utils/open_sans_font.dart';

final _authStore = AuthStore();

class EventButton extends StatelessWidget with OpenSansFont {
  final EventDetails event;

  const EventButton({Key? key, required this.event}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (_authStore.isGuest() ||
        (event.attendeeLimit != 0 &&
            event.attendees.length == event.attendeeLimit)) {
      return const DisabledEventAttendButton();
    }

    if (event.attendees
            .indexWhere((element) => element.id == _authStore.getId()) >
        -1) {
      return EventCancelAttendButton(event: event);
    }

    return EventAttendButton(event: event);
  }
}
