//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:proximity/screens/event_screen/widgets/delete_event_button.dart';
import 'package:proximity/screens/event_screen/widgets/event_button.dart';
import 'package:proximity/screens/event_screen/widgets/event_description.dart';
import 'package:proximity/screens/event_screen/widgets/event_info.dart';
import 'package:proximity/screens/event_screen/widgets/event_title.dart';
import 'package:proximity/stores/auth_store.dart';
import 'package:proximity/stores/event_store.dart';
import 'package:proximity/utils/open_sans_font.dart';

final _authStore = AuthStore();
final _eventStore = EventStore();

class EventScreen extends StatelessWidget with OpenSansFont {
  final String eventId;

  const EventScreen({Key? key, required this.eventId}) : super(key: key);

  Future<void> onRefresh() async {
    _eventStore.getById(eventId);
  }

  @override
  Widget build(BuildContext context) {
    if (_eventStore.event == null || _eventStore.event?.id != eventId) {
      onRefresh();
    }

    return Observer(
      builder: (context) => Scaffold(
        appBar: AppBar(
          title: Text(
            "Event information",
            style: openSansFont(
              fontWeight: FontWeight.w700,
              fontSize: 18,
            ),
          ),
          actions: !_authStore.isGuest() &&
                  _authStore.getId() == _eventStore.event?.organizer.id
              ? [DeleteEventButton(eventId: eventId)]
              : [],
          backgroundColor: Colors.indigoAccent,
        ),
        body: Observer(builder: (context) {
          if (_eventStore.isLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          if (!_eventStore.isLoading && _eventStore.event == null) {
            return RefreshIndicator(
              onRefresh: onRefresh,
              child: LayoutBuilder(
                builder: (context, constraints) => ListView(
                  children: [
                    Container(
                      padding: const EdgeInsets.all(20),
                      constraints: BoxConstraints(
                        minHeight: constraints.maxHeight,
                      ),
                      child: Center(
                        child: Text(
                          "Failed to get event details.",
                          style: openSansFont(
                            color: Colors.black,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }

          return ListView(
            children: [
              FadeInImage(
                image: NetworkImage(_eventStore.event!.pictureUrl),
                placeholder: const AssetImage("assets/event_placeholder.webp"),
              ),
              EventTitle(event: _eventStore.event!),
              EventInfo(event: _eventStore.event!),
              EventDescription(event: _eventStore.event!),
              EventButton(event: _eventStore.event!),
            ],
          );
        }),
      ),
    );
  }
}
