//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:proximity/models/user_details.dart';
import 'package:proximity/screens/profile_screen/widgets/event_grid.dart';
import 'package:proximity/screens/profile_screen/widgets/profile_section.dart';
import 'package:proximity/utils/open_sans_font.dart';

class ProfileEvents extends StatelessWidget with OpenSansFont {
  final UserDetails user;

  const ProfileEvents({Key? key, required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ProfileSection(
      children: [
        Text(
          "Organized events",
          style: openSansFont(
            color: Colors.black,
            fontSize: 30,
            fontWeight: FontWeight.w800,
          ),
        ),
        const Padding(padding: EdgeInsets.only(bottom: 10)),
        user.organizedEvents.isEmpty
            ? Text(
                "You haven't organized any event.",
                style: openSansFont(
                  color: Colors.black,
                ),
              )
            : EventGrid(events: user.organizedEvents),
        const Padding(padding: EdgeInsets.only(bottom: 35)),
        Text(
          "Attended events",
          style: openSansFont(
            color: Colors.black,
            fontSize: 30,
            fontWeight: FontWeight.w800,
          ),
        ),
        const Padding(padding: EdgeInsets.only(bottom: 10)),
        user.attendedEvents.isEmpty
            ? Text(
                "You haven't attended any event.",
                style: openSansFont(
                  color: Colors.black,
                ),
              )
            : EventGrid(events: user.attendedEvents),
      ],
    );
  }
}
