//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:proximity/models/user_details.dart';
import 'package:proximity/screens/profile_screen/widgets/logout_button.dart';
import 'package:proximity/utils/open_sans_font.dart';

class ProfileHeader extends StatelessWidget with OpenSansFont {
  final UserDetails user;

  const ProfileHeader({Key? key, required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.3,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(60),
            child: FadeInImage(
              image: NetworkImage(user.avatarUrl!),
              placeholder: const AssetImage("assets/avatar_placeholder.webp"),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 45),
          child: Column(
            children: [
              Text(
                "@${user.username}",
                style: openSansFont(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.amber,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    minimumSize:
                        Size(MediaQuery.of(context).size.width * 0.35, 35),
                  ),
                  onPressed: () {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        duration: Duration(milliseconds: 750),
                        content: Text("Not implemented."),
                      ),
                    );
                  },
                  child: Text(
                    "Edit profile",
                    style: openSansFont(color: Colors.black),
                  ),
                ),
              ),
              const LogoutButton(),
            ],
          ),
        ),
      ],
    );
  }
}
