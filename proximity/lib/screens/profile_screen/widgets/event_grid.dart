//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:proximity/models/event.dart';
import 'package:proximity/screens/profile_screen/widgets/event_grid_card.dart';

class EventGrid extends StatelessWidget {
  final List<Event> events;

  const EventGrid({super.key, required this.events});

  @override
  Widget build(BuildContext context) {
    final List<Widget> children = events
        .map(
          (event) => EventGridCard(
            event: event,
          ),
        )
        .toList();

    return GridView.count(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      primary: false,
      crossAxisSpacing: 8,
      mainAxisSpacing: 8,
      crossAxisCount: 2,
      children: children,
    );
  }
}
