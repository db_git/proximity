//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:proximity/models/user_details.dart';
import 'package:proximity/screens/profile_screen/widgets/profile_events.dart';
import 'package:proximity/screens/profile_screen/widgets/profile_header.dart';
import 'package:proximity/utils/open_sans_font.dart';

class ProfileScreen extends StatelessWidget with OpenSansFont {
  final UserDetails user;

  ProfileScreen({Key? key, required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const padding = Padding(padding: EdgeInsets.only(top: 35));

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.indigoAccent,
        title: Text(
          "Profile information",
          style: openSansFont(
            fontWeight: FontWeight.w700,
            fontSize: 18,
          ),
        ),
      ),
      body: ListView(
        children: [
          padding,
          ProfileHeader(user: user),
          padding,
          ProfileEvents(user: user),
          padding,
        ],
      ),
    );
  }
}
