// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_event_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$CreateEventStore on _CreateEventStore, Store {
  Computed<bool>? _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading,
              name: '_CreateEventStore.isLoading'))
          .value;
  Computed<String?>? _$titleComputed;

  @override
  String? get title => (_$titleComputed ??=
          Computed<String?>(() => super.title, name: '_CreateEventStore.title'))
      .value;
  Computed<String?>? _$titleErrorComputed;

  @override
  String? get titleError =>
      (_$titleErrorComputed ??= Computed<String?>(() => super.titleError,
              name: '_CreateEventStore.titleError'))
          .value;
  Computed<String?>? _$subtitleComputed;

  @override
  String? get subtitle =>
      (_$subtitleComputed ??= Computed<String?>(() => super.subtitle,
              name: '_CreateEventStore.subtitle'))
          .value;
  Computed<String?>? _$subtitleErrorComputed;

  @override
  String? get subtitleError =>
      (_$subtitleErrorComputed ??= Computed<String?>(() => super.subtitleError,
              name: '_CreateEventStore.subtitleError'))
          .value;
  Computed<DateTime?>? _$dateComputed;

  @override
  DateTime? get date => (_$dateComputed ??=
          Computed<DateTime?>(() => super.date, name: '_CreateEventStore.date'))
      .value;
  Computed<String?>? _$dateErrorComputed;

  @override
  String? get dateError =>
      (_$dateErrorComputed ??= Computed<String?>(() => super.dateError,
              name: '_CreateEventStore.dateError'))
          .value;
  Computed<String?>? _$pictureUrlComputed;

  @override
  String? get pictureUrl =>
      (_$pictureUrlComputed ??= Computed<String?>(() => super.pictureUrl,
              name: '_CreateEventStore.pictureUrl'))
          .value;
  Computed<String?>? _$pictureUrlErrorComputed;

  @override
  String? get pictureUrlError => (_$pictureUrlErrorComputed ??=
          Computed<String?>(() => super.pictureUrlError,
              name: '_CreateEventStore.pictureUrlError'))
      .value;
  Computed<String?>? _$locationComputed;

  @override
  String? get location =>
      (_$locationComputed ??= Computed<String?>(() => super.location,
              name: '_CreateEventStore.location'))
          .value;
  Computed<String?>? _$locationErrorComputed;

  @override
  String? get locationError =>
      (_$locationErrorComputed ??= Computed<String?>(() => super.locationError,
              name: '_CreateEventStore.locationError'))
          .value;
  Computed<String?>? _$descriptionComputed;

  @override
  String? get description =>
      (_$descriptionComputed ??= Computed<String?>(() => super.description,
              name: '_CreateEventStore.description'))
          .value;
  Computed<String?>? _$descriptionErrorComputed;

  @override
  String? get descriptionError => (_$descriptionErrorComputed ??=
          Computed<String?>(() => super.descriptionError,
              name: '_CreateEventStore.descriptionError'))
      .value;
  Computed<int?>? _$attendeeLimitComputed;

  @override
  int? get attendeeLimit =>
      (_$attendeeLimitComputed ??= Computed<int?>(() => super.attendeeLimit,
              name: '_CreateEventStore.attendeeLimit'))
          .value;
  Computed<String?>? _$attendeeLimitErrorComputed;

  @override
  String? get attendeeLimitError => (_$attendeeLimitErrorComputed ??=
          Computed<String?>(() => super.attendeeLimitError,
              name: '_CreateEventStore.attendeeLimitError'))
      .value;
  Computed<int?>? _$priceComputed;

  @override
  int? get price => (_$priceComputed ??=
          Computed<int?>(() => super.price, name: '_CreateEventStore.price'))
      .value;
  Computed<String?>? _$priceErrorComputed;

  @override
  String? get priceError =>
      (_$priceErrorComputed ??= Computed<String?>(() => super.priceError,
              name: '_CreateEventStore.priceError'))
          .value;
  Computed<String?>? _$organizerComputed;

  @override
  String? get organizer =>
      (_$organizerComputed ??= Computed<String?>(() => super.organizer,
              name: '_CreateEventStore.organizer'))
          .value;
  Computed<bool>? _$isValidComputed;

  @override
  bool get isValid => (_$isValidComputed ??= Computed<bool>(() => super.isValid,
          name: '_CreateEventStore.isValid'))
      .value;

  late final _$_loadingAtom =
      Atom(name: '_CreateEventStore._loading', context: context);

  @override
  bool get _loading {
    _$_loadingAtom.reportRead();
    return super._loading;
  }

  @override
  set _loading(bool value) {
    _$_loadingAtom.reportWrite(value, super._loading, () {
      super._loading = value;
    });
  }

  late final _$_titleAtom =
      Atom(name: '_CreateEventStore._title', context: context);

  @override
  String? get _title {
    _$_titleAtom.reportRead();
    return super._title;
  }

  @override
  set _title(String? value) {
    _$_titleAtom.reportWrite(value, super._title, () {
      super._title = value;
    });
  }

  late final _$_titleErrorAtom =
      Atom(name: '_CreateEventStore._titleError', context: context);

  @override
  String? get _titleError {
    _$_titleErrorAtom.reportRead();
    return super._titleError;
  }

  @override
  set _titleError(String? value) {
    _$_titleErrorAtom.reportWrite(value, super._titleError, () {
      super._titleError = value;
    });
  }

  late final _$_subtitleAtom =
      Atom(name: '_CreateEventStore._subtitle', context: context);

  @override
  String? get _subtitle {
    _$_subtitleAtom.reportRead();
    return super._subtitle;
  }

  @override
  set _subtitle(String? value) {
    _$_subtitleAtom.reportWrite(value, super._subtitle, () {
      super._subtitle = value;
    });
  }

  late final _$_subtitleErrorAtom =
      Atom(name: '_CreateEventStore._subtitleError', context: context);

  @override
  String? get _subtitleError {
    _$_subtitleErrorAtom.reportRead();
    return super._subtitleError;
  }

  @override
  set _subtitleError(String? value) {
    _$_subtitleErrorAtom.reportWrite(value, super._subtitleError, () {
      super._subtitleError = value;
    });
  }

  late final _$_dateAtom =
      Atom(name: '_CreateEventStore._date', context: context);

  @override
  DateTime? get _date {
    _$_dateAtom.reportRead();
    return super._date;
  }

  @override
  set _date(DateTime? value) {
    _$_dateAtom.reportWrite(value, super._date, () {
      super._date = value;
    });
  }

  late final _$_dateErrorAtom =
      Atom(name: '_CreateEventStore._dateError', context: context);

  @override
  String? get _dateError {
    _$_dateErrorAtom.reportRead();
    return super._dateError;
  }

  @override
  set _dateError(String? value) {
    _$_dateErrorAtom.reportWrite(value, super._dateError, () {
      super._dateError = value;
    });
  }

  late final _$_pictureUrlAtom =
      Atom(name: '_CreateEventStore._pictureUrl', context: context);

  @override
  String? get _pictureUrl {
    _$_pictureUrlAtom.reportRead();
    return super._pictureUrl;
  }

  @override
  set _pictureUrl(String? value) {
    _$_pictureUrlAtom.reportWrite(value, super._pictureUrl, () {
      super._pictureUrl = value;
    });
  }

  late final _$_pictureUrlErrorAtom =
      Atom(name: '_CreateEventStore._pictureUrlError', context: context);

  @override
  String? get _pictureUrlError {
    _$_pictureUrlErrorAtom.reportRead();
    return super._pictureUrlError;
  }

  @override
  set _pictureUrlError(String? value) {
    _$_pictureUrlErrorAtom.reportWrite(value, super._pictureUrlError, () {
      super._pictureUrlError = value;
    });
  }

  late final _$_locationAtom =
      Atom(name: '_CreateEventStore._location', context: context);

  @override
  String? get _location {
    _$_locationAtom.reportRead();
    return super._location;
  }

  @override
  set _location(String? value) {
    _$_locationAtom.reportWrite(value, super._location, () {
      super._location = value;
    });
  }

  late final _$_locationErrorAtom =
      Atom(name: '_CreateEventStore._locationError', context: context);

  @override
  String? get _locationError {
    _$_locationErrorAtom.reportRead();
    return super._locationError;
  }

  @override
  set _locationError(String? value) {
    _$_locationErrorAtom.reportWrite(value, super._locationError, () {
      super._locationError = value;
    });
  }

  late final _$_descriptionAtom =
      Atom(name: '_CreateEventStore._description', context: context);

  @override
  String? get _description {
    _$_descriptionAtom.reportRead();
    return super._description;
  }

  @override
  set _description(String? value) {
    _$_descriptionAtom.reportWrite(value, super._description, () {
      super._description = value;
    });
  }

  late final _$_descriptionErrorAtom =
      Atom(name: '_CreateEventStore._descriptionError', context: context);

  @override
  String? get _descriptionError {
    _$_descriptionErrorAtom.reportRead();
    return super._descriptionError;
  }

  @override
  set _descriptionError(String? value) {
    _$_descriptionErrorAtom.reportWrite(value, super._descriptionError, () {
      super._descriptionError = value;
    });
  }

  late final _$_attendeeLimitAtom =
      Atom(name: '_CreateEventStore._attendeeLimit', context: context);

  @override
  int? get _attendeeLimit {
    _$_attendeeLimitAtom.reportRead();
    return super._attendeeLimit;
  }

  @override
  set _attendeeLimit(int? value) {
    _$_attendeeLimitAtom.reportWrite(value, super._attendeeLimit, () {
      super._attendeeLimit = value;
    });
  }

  late final _$_attendeeLimitErrorAtom =
      Atom(name: '_CreateEventStore._attendeeLimitError', context: context);

  @override
  String? get _attendeeLimitError {
    _$_attendeeLimitErrorAtom.reportRead();
    return super._attendeeLimitError;
  }

  @override
  set _attendeeLimitError(String? value) {
    _$_attendeeLimitErrorAtom.reportWrite(value, super._attendeeLimitError, () {
      super._attendeeLimitError = value;
    });
  }

  late final _$_priceAtom =
      Atom(name: '_CreateEventStore._price', context: context);

  @override
  int? get _price {
    _$_priceAtom.reportRead();
    return super._price;
  }

  @override
  set _price(int? value) {
    _$_priceAtom.reportWrite(value, super._price, () {
      super._price = value;
    });
  }

  late final _$_priceErrorAtom =
      Atom(name: '_CreateEventStore._priceError', context: context);

  @override
  String? get _priceError {
    _$_priceErrorAtom.reportRead();
    return super._priceError;
  }

  @override
  set _priceError(String? value) {
    _$_priceErrorAtom.reportWrite(value, super._priceError, () {
      super._priceError = value;
    });
  }

  late final _$_organizerAtom =
      Atom(name: '_CreateEventStore._organizer', context: context);

  @override
  String? get _organizer {
    _$_organizerAtom.reportRead();
    return super._organizer;
  }

  @override
  set _organizer(String? value) {
    _$_organizerAtom.reportWrite(value, super._organizer, () {
      super._organizer = value;
    });
  }

  late final _$createAsyncAction =
      AsyncAction('_CreateEventStore.create', context: context);

  @override
  Future<void> create() {
    return _$createAsyncAction.run(() => super.create());
  }

  late final _$_CreateEventStoreActionController =
      ActionController(name: '_CreateEventStore', context: context);

  @override
  void setTitle(String? title) {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore.setTitle');
    try {
      return super.setTitle(title);
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void _validateTitle() {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore._validateTitle');
    try {
      return super._validateTitle();
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setSubtitle(String? subtitle) {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore.setSubtitle');
    try {
      return super.setSubtitle(subtitle);
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void _validateSubtitle() {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore._validateSubtitle');
    try {
      return super._validateSubtitle();
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setDate(DateTime? date) {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore.setDate');
    try {
      return super.setDate(date);
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void _validateDate() {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore._validateDate');
    try {
      return super._validateDate();
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setPictureUrl(String? pictureUrl) {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore.setPictureUrl');
    try {
      return super.setPictureUrl(pictureUrl);
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void _validatePictureUrl() {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore._validatePictureUrl');
    try {
      return super._validatePictureUrl();
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setLocation(String? location) {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore.setLocation');
    try {
      return super.setLocation(location);
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void _validateLocation() {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore._validateLocation');
    try {
      return super._validateLocation();
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setDescription(String? description) {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore.setDescription');
    try {
      return super.setDescription(description);
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void _validateDescription() {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore._validateDescription');
    try {
      return super._validateDescription();
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setLimt(int? limit) {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore.setLimt');
    try {
      return super.setLimt(limit);
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void _validateLimit() {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore._validateLimit');
    try {
      return super._validateLimit();
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setPrice(int? price) {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore.setPrice');
    try {
      return super.setPrice(price);
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void _validatePrice() {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore._validatePrice');
    try {
      return super._validatePrice();
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setOrganizer(String? organizer) {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore.setOrganizer');
    try {
      return super.setOrganizer(organizer);
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearInputs() {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore.clearInputs');
    try {
      return super.clearInputs();
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearErrors() {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore.clearErrors');
    try {
      return super.clearErrors();
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void validate() {
    final _$actionInfo = _$_CreateEventStoreActionController.startAction(
        name: '_CreateEventStore.validate');
    try {
      return super.validate();
    } finally {
      _$_CreateEventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
isLoading: ${isLoading},
title: ${title},
titleError: ${titleError},
subtitle: ${subtitle},
subtitleError: ${subtitleError},
date: ${date},
dateError: ${dateError},
pictureUrl: ${pictureUrl},
pictureUrlError: ${pictureUrlError},
location: ${location},
locationError: ${locationError},
description: ${description},
descriptionError: ${descriptionError},
attendeeLimit: ${attendeeLimit},
attendeeLimitError: ${attendeeLimitError},
price: ${price},
priceError: ${priceError},
organizer: ${organizer},
isValid: ${isValid}
    ''';
  }
}
