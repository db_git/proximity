// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$ProfileStore on _ProfileStore, Store {
  Computed<UserDetails?>? _$userComputed;

  @override
  UserDetails? get user => (_$userComputed ??=
          Computed<UserDetails?>(() => super.user, name: '_ProfileStore.user'))
      .value;
  Computed<bool>? _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading,
              name: '_ProfileStore.isLoading'))
          .value;

  late final _$_userAtom = Atom(name: '_ProfileStore._user', context: context);

  @override
  UserDetails? get _user {
    _$_userAtom.reportRead();
    return super._user;
  }

  @override
  set _user(UserDetails? value) {
    _$_userAtom.reportWrite(value, super._user, () {
      super._user = value;
    });
  }

  late final _$_loadingAtom =
      Atom(name: '_ProfileStore._loading', context: context);

  @override
  bool get _loading {
    _$_loadingAtom.reportRead();
    return super._loading;
  }

  @override
  set _loading(bool value) {
    _$_loadingAtom.reportWrite(value, super._loading, () {
      super._loading = value;
    });
  }

  late final _$getByIdAsyncAction =
      AsyncAction('_ProfileStore.getById', context: context);

  @override
  Future<void> getById(String userId) {
    return _$getByIdAsyncAction.run(() => super.getById(userId));
  }

  @override
  String toString() {
    return '''
user: ${user},
isLoading: ${isLoading}
    ''';
  }
}
