//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:http/http.dart' as http;
import 'package:mobx/mobx.dart';
import 'package:proximity/constants/api.dart';
import 'package:proximity/stores/auth_store.dart';

part 'delete_event_store.g.dart';

final _authStore = AuthStore();

class DeleteEventStore extends _DeleteEventStore with _$DeleteEventStore {
  static final DeleteEventStore _instance = DeleteEventStore._();

  DeleteEventStore._();

  factory DeleteEventStore() {
    return _instance;
  }
}

abstract class _DeleteEventStore with Store {
  @observable
  bool _loading = false;

  @computed
  bool get isLoading => _loading;

  @action
  Future<void> delete(String eventId) async {
    _loading = true;

    final http.Client client = http.Client();

    try {
      await http.delete(
        Uri.parse("${ApiPaths.eventEndpoint}/$eventId"),
        headers: {
          "Authorization": "Bearer ${_authStore.jwt}",
        },
      );
    } catch (_) {
      // ignored
    } finally {
      client.close();
      _loading = false;
    }
  }
}
