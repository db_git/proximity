//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:http/http.dart' as http;
import 'package:mobx/mobx.dart';
import 'package:proximity/constants/api.dart';

import 'auth_store.dart';

part 'create_event_store.g.dart';

const int pageSize = 5;

final _authStore = AuthStore();

class CreateEventStore extends _CreateEventStore with _$CreateEventStore {
  static final CreateEventStore _instance = CreateEventStore._();

  CreateEventStore._();

  factory CreateEventStore() {
    return _instance;
  }
}

abstract class _CreateEventStore with Store {
  @observable
  bool _loading = false;

  @computed
  bool get isLoading => _loading;

  @observable
  String? _title;

  @computed
  String? get title => _title;

  @action
  void setTitle(String? title) {
    _title = title;
  }

  @observable
  String? _titleError;

  @computed
  String? get titleError => _titleError;

  @action
  void _validateTitle() {
    if (_title == null) {
      _titleError = "Title can't be empty.";
      return;
    }

    if (_title!.trim() == "") {
      _titleError = "Title can't be empty.";
      return;
    }

    if (_title!.trim().length < 4) {
      _titleError = "Minimum length is 4 characters.";
      return;
    }

    if (_title!.trim().length > 32) {
      _titleError = "Maximum length is 32 characters.";
      return;
    }

    _titleError = null;
  }

  @observable
  String? _subtitle;

  @computed
  String? get subtitle => _subtitle;

  @action
  void setSubtitle(String? subtitle) {
    _subtitle = subtitle;
  }

  @observable
  String? _subtitleError;

  @computed
  String? get subtitleError => _subtitleError;

  @action
  void _validateSubtitle() {
    if (_subtitle == null) {
      _subtitleError = null;
      return;
    }

    if (_subtitle != null && _subtitle!.trim() == "") {
      _subtitleError = null;
      return;
    }

    if (_subtitle != null && _subtitle!.trim().length < 4) {
      _subtitleError = "Minimum length is 4 characters.";
      return;
    }

    if (_subtitle != null && _subtitle!.trim().length > 32) {
      _subtitleError = "Maximum length is 32 characters.";
      return;
    }

    _subtitleError = null;
  }

  @observable
  DateTime? _date;

  @computed
  DateTime? get date => _date;

  @action
  void setDate(DateTime? date) {
    _date = date;
  }

  @observable
  String? _dateError;

  @computed
  String? get dateError => _dateError;

  @action
  void _validateDate() {
    if (_date == null) {
      _dateError = "Date can't be empty.";
      return;
    }

    if (_date!.isBefore(DateTime.now())) {
      _dateError = "Date can't be in past.";
    }

    _dateError = null;
  }

  @observable
  String? _pictureUrl;

  @computed
  String? get pictureUrl => _pictureUrl;

  @action
  void setPictureUrl(String? pictureUrl) {
    _pictureUrl = pictureUrl;
  }

  @observable
  String? _pictureUrlError;

  @computed
  String? get pictureUrlError => _pictureUrlError;

  @action
  void _validatePictureUrl() {
    if (_pictureUrl == null) {
      _pictureUrlError = "URL can't be empty.";
      return;
    }

    if (_pictureUrl!.trim() == "") {
      _pictureUrlError = "URL can't be empty.";
      return;
    }

    if (_pictureUrl!.trim().contains(' ')) {
      _pictureUrlError = "URL can't have spaces.";
      return;
    }

    if (_pictureUrl!.trim().length < 12) {
      _pictureUrlError = "Minimum length is 12 characters.";
      return;
    }

    if (_pictureUrl!.trim().length > 2048) {
      _pictureUrlError = "Maximum length is 2048 characters.";
      return;
    }

    if (!_pictureUrl!.trim().startsWith("https://")) {
      _pictureUrlError = "Not a valid URL.";
      return;
    }

    _pictureUrlError = null;
  }

  @observable
  String? _location;

  @computed
  String? get location => _location;

  @action
  void setLocation(String? location) {
    _location = location;
  }

  @observable
  String? _locationError;

  @computed
  String? get locationError => _locationError;

  @action
  void _validateLocation() {
    if (_location == null) {
      _locationError = "Location can't be empty.";
      return;
    }

    if (_location!.trim() == "") {
      _locationError = "Location can't be empty.";
      return;
    }

    if (_location!.trim().length < 8) {
      _locationError = "Minimum length is 8 characters.";
      return;
    }

    if (_location!.trim().length > 128) {
      _locationError = "Maximum length is 128 characters.";
      return;
    }

    _locationError = null;
  }

  @observable
  String? _description;

  @computed
  String? get description => _description;

  @action
  void setDescription(String? description) {
    _description = description;
  }

  @observable
  String? _descriptionError;

  @computed
  String? get descriptionError => _descriptionError;

  @action
  void _validateDescription() {
    if (_description == null) {
      _descriptionError = "Description can't be empty.";
      return;
    }

    if (_description!.trim() == "") {
      _descriptionError = "Description can't be empty.";
      return;
    }

    if (_description!.trim().length < 8) {
      _descriptionError = "Minimum length is 8 characters.";
      return;
    }

    if (_description!.trim().length > 2048) {
      _descriptionError = "Maximum length is 2048 characters.";
      return;
    }

    _descriptionError = null;
  }

  @observable
  int? _attendeeLimit;

  @computed
  int? get attendeeLimit => _attendeeLimit;

  @action
  void setLimt(int? limit) {
    _attendeeLimit = limit;
  }

  @observable
  String? _attendeeLimitError;

  @computed
  String? get attendeeLimitError => _attendeeLimitError;

  @action
  void _validateLimit() {
    if (_attendeeLimit == null) {
      _attendeeLimitError = "Limit can't be empty.";
      return;
    }

    if (_attendeeLimit! < 0) {
      _attendeeLimitError = "Limit can't be negative.";
      return;
    }

    if (_attendeeLimit! > 1000) {
      _attendeeLimitError = "Maximum limit is 1000.";
      return;
    }

    _attendeeLimitError = null;
  }

  @observable
  int? _price;

  @computed
  int? get price => _price;

  @action
  void setPrice(int? price) {
    _price = price;
  }

  @observable
  String? _priceError;

  @computed
  String? get priceError => _priceError;

  @action
  void _validatePrice() {
    if (_price == null) {
      _priceError = "Price can't be empty.";
      return;
    }

    if (_price! < 0) {
      _priceError = "Price can't be negative.";
      return;
    }

    if (_price! > 1000) {
      _priceError = "Price limit is 1000 EUR.";
      return;
    }

    _priceError = null;
  }

  @observable
  String? _organizer;

  @computed
  String? get organizer => _organizer;

  @action
  void setOrganizer(String? organizer) {
    _organizer = organizer;
  }

  @action
  void clearInputs() {
    _title = null;
    _subtitle = null;
    _date = null;
    _pictureUrl = null;
    _location = null;
    _description = null;
    _attendeeLimit = null;
    _price = null;
  }

  @action
  void clearErrors() {
    _titleError = null;
    _subtitleError = null;
    _dateError = null;
    _pictureUrlError = null;
    _locationError = null;
    _descriptionError = null;
    _attendeeLimitError = null;
    _priceError = null;
  }

  @action
  void validate() {
    _validateTitle();
    _validateSubtitle();
    _validateDate();
    _validatePictureUrl();
    _validateLocation();
    _validateDescription();
    _validateLimit();
    _validatePrice();
  }

  @computed
  bool get isValid =>
      _titleError == null &&
      _subtitleError == null &&
      _dateError == null &&
      _pictureUrlError == null &&
      _locationError == null &&
      _descriptionError == null &&
      _attendeeLimitError == null &&
      _priceError == null;

  @action
  Future<void> create() async {
    _loading = true;

    final http.Client client = http.Client();

    await client.post(
      Uri.parse(ApiPaths.eventEndpoint),
      body: """
        {
          "title": "$title",
          ${subtitle == null || subtitle!.trim() == "" ? "\"subtitle\": null," : "\"subtitle\": \"$subtitle\","}
          "date": "${date!.toUtc().toIso8601String()}",
          "pictureUrl": "$pictureUrl",
          "location": "$location",
          "description": "$description",
          "attendeeLimit": $attendeeLimit,
          "price": $price,
          "organizer": "${_authStore.getId()}"
        }
      """,
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer ${_authStore.jwt}",
      },
    );

    client.close();
    _loading = false;
  }
}
