//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:mobx/mobx.dart';
import 'package:proximity/constants/api.dart';

part 'auth_store.g.dart';

class AuthStore extends _AuthStore with _$AuthStore {
  static final AuthStore _instance = AuthStore._();

  AuthStore._();

  factory AuthStore() {
    return _instance;
  }
}

abstract class _AuthStore with Store {
  @observable
  String? _jwt;

  @observable
  bool _loading = false;

  @computed
  String? get jwt {
    return _jwt;
  }

  @computed
  bool get isLoading {
    return _loading;
  }

  bool isLoggedIn() {
    return jwt != null;
  }

  bool isGuest() {
    return jwt == "guest";
  }

  @action
  void loginAsGuest() {
    _jwt = "guest";
  }

  @action
  void logout() {
    _jwt = null;
  }

  @action
  Future<void> login(String email, String password) async {
    _loading = true;

    final client = http.Client();
    final body = """
      {
        "email": "$email",
        "password": "$password"
      }
    """;

    try {
      final response = await client.post(
        Uri.parse(ApiPaths.loginEndpoint),
        body: body,
        headers: {"Content-Type": "application/json"},
      );
      _jwt = jsonDecode(response.body)["token"];
    } catch (_) {
      _jwt = null;
    } finally {
      client.close();
    }

    _loading = false;
  }

  @action
  Future<void> register(String username, String email, String password) async {
    _loading = true;

    final Client client = http.Client();
    final String body = """
    {
      "username": "$username",
      "email": "$email",
      "password": "$password"
    }
    """;

    try {
      final Response response = await client.post(
        Uri.parse(ApiPaths.registerEndpoint),
        body: body,
        headers: {"Content-Type": "application/json"},
      );

      _jwt = jsonDecode(response.body)["token"];
    } catch (_) {
      _jwt = null;
    } finally {
      client.close();
    }

    _loading = false;
  }

  @action
  String? getId() {
    if (_jwt == null || _jwt == "guest") return null;
    return JwtDecoder.decode(_jwt!)["sub"];
  }
}
