// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$AuthStore on _AuthStore, Store {
  Computed<String?>? _$jwtComputed;

  @override
  String? get jwt => (_$jwtComputed ??=
          Computed<String?>(() => super.jwt, name: '_AuthStore.jwt'))
      .value;
  Computed<bool>? _$isLoadingComputed;

  @override
  bool get isLoading => (_$isLoadingComputed ??=
          Computed<bool>(() => super.isLoading, name: '_AuthStore.isLoading'))
      .value;

  late final _$_jwtAtom = Atom(name: '_AuthStore._jwt', context: context);

  @override
  String? get _jwt {
    _$_jwtAtom.reportRead();
    return super._jwt;
  }

  @override
  set _jwt(String? value) {
    _$_jwtAtom.reportWrite(value, super._jwt, () {
      super._jwt = value;
    });
  }

  late final _$_loadingAtom =
      Atom(name: '_AuthStore._loading', context: context);

  @override
  bool get _loading {
    _$_loadingAtom.reportRead();
    return super._loading;
  }

  @override
  set _loading(bool value) {
    _$_loadingAtom.reportWrite(value, super._loading, () {
      super._loading = value;
    });
  }

  late final _$loginAsyncAction =
      AsyncAction('_AuthStore.login', context: context);

  @override
  Future<void> login(String email, String password) {
    return _$loginAsyncAction.run(() => super.login(email, password));
  }

  late final _$registerAsyncAction =
      AsyncAction('_AuthStore.register', context: context);

  @override
  Future<void> register(String username, String email, String password) {
    return _$registerAsyncAction
        .run(() => super.register(username, email, password));
  }

  late final _$_AuthStoreActionController =
      ActionController(name: '_AuthStore', context: context);

  @override
  void loginAsGuest() {
    final _$actionInfo = _$_AuthStoreActionController.startAction(
        name: '_AuthStore.loginAsGuest');
    try {
      return super.loginAsGuest();
    } finally {
      _$_AuthStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void logout() {
    final _$actionInfo =
        _$_AuthStoreActionController.startAction(name: '_AuthStore.logout');
    try {
      return super.logout();
    } finally {
      _$_AuthStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? getId() {
    final _$actionInfo =
        _$_AuthStoreActionController.startAction(name: '_AuthStore.getId');
    try {
      return super.getId();
    } finally {
      _$_AuthStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
jwt: ${jwt},
isLoading: ${isLoading}
    ''';
  }
}
