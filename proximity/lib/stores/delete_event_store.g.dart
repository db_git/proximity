// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'delete_event_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$DeleteEventStore on _DeleteEventStore, Store {
  Computed<bool>? _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading,
              name: '_DeleteEventStore.isLoading'))
          .value;

  late final _$_loadingAtom =
      Atom(name: '_DeleteEventStore._loading', context: context);

  @override
  bool get _loading {
    _$_loadingAtom.reportRead();
    return super._loading;
  }

  @override
  set _loading(bool value) {
    _$_loadingAtom.reportWrite(value, super._loading, () {
      super._loading = value;
    });
  }

  late final _$deleteAsyncAction =
      AsyncAction('_DeleteEventStore.delete', context: context);

  @override
  Future<void> delete(String eventId) {
    return _$deleteAsyncAction.run(() => super.delete(eventId));
  }

  @override
  String toString() {
    return '''
isLoading: ${isLoading}
    ''';
  }
}
