// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'registration_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$RegistrationStore on _RegistrationStore, Store {
  Computed<ActiveForm>? _$activeFormComputed;

  @override
  ActiveForm get activeForm =>
      (_$activeFormComputed ??= Computed<ActiveForm>(() => super.activeForm,
              name: '_RegistrationStore.activeForm'))
          .value;
  Computed<String?>? _$usernameComputed;

  @override
  String? get username =>
      (_$usernameComputed ??= Computed<String?>(() => super.username,
              name: '_RegistrationStore.username'))
          .value;
  Computed<String?>? _$usernameErrorComputed;

  @override
  String? get usernameError =>
      (_$usernameErrorComputed ??= Computed<String?>(() => super.usernameError,
              name: '_RegistrationStore.usernameError'))
          .value;
  Computed<String?>? _$passwordComputed;

  @override
  String? get password =>
      (_$passwordComputed ??= Computed<String?>(() => super.password,
              name: '_RegistrationStore.password'))
          .value;
  Computed<String?>? _$passwordErrorComputed;

  @override
  String? get passwordError =>
      (_$passwordErrorComputed ??= Computed<String?>(() => super.passwordError,
              name: '_RegistrationStore.passwordError'))
          .value;
  Computed<String?>? _$passwordRepeatComputed;

  @override
  String? get passwordRepeat => (_$passwordRepeatComputed ??= Computed<String?>(
          () => super.passwordRepeat,
          name: '_RegistrationStore.passwordRepeat'))
      .value;
  Computed<String?>? _$passwordRepeatErrorComputed;

  @override
  String? get passwordRepeatError => (_$passwordRepeatErrorComputed ??=
          Computed<String?>(() => super.passwordRepeatError,
              name: '_RegistrationStore.passwordRepeatError'))
      .value;
  Computed<String?>? _$emailComputed;

  @override
  String? get email => (_$emailComputed ??= Computed<String?>(() => super.email,
          name: '_RegistrationStore.email'))
      .value;
  Computed<String?>? _$emailErrorComputed;

  @override
  String? get emailError =>
      (_$emailErrorComputed ??= Computed<String?>(() => super.emailError,
              name: '_RegistrationStore.emailError'))
          .value;
  Computed<bool>? _$hasRegistrationErrorsComputed;

  @override
  bool get hasRegistrationErrors => (_$hasRegistrationErrorsComputed ??=
          Computed<bool>(() => super.hasRegistrationErrors,
              name: '_RegistrationStore.hasRegistrationErrors'))
      .value;
  Computed<bool>? _$hasLoginErrorsComputed;

  @override
  bool get hasLoginErrors =>
      (_$hasLoginErrorsComputed ??= Computed<bool>(() => super.hasLoginErrors,
              name: '_RegistrationStore.hasLoginErrors'))
          .value;
  Computed<bool>? _$isRegistrationValidComputed;

  @override
  bool get isRegistrationValid => (_$isRegistrationValidComputed ??=
          Computed<bool>(() => super.isRegistrationValid,
              name: '_RegistrationStore.isRegistrationValid'))
      .value;
  Computed<bool>? _$isLoginValidComputed;

  @override
  bool get isLoginValid =>
      (_$isLoginValidComputed ??= Computed<bool>(() => super.isLoginValid,
              name: '_RegistrationStore.isLoginValid'))
          .value;

  late final _$_usernameAtom =
      Atom(name: '_RegistrationStore._username', context: context);

  @override
  String? get _username {
    _$_usernameAtom.reportRead();
    return super._username;
  }

  @override
  set _username(String? value) {
    _$_usernameAtom.reportWrite(value, super._username, () {
      super._username = value;
    });
  }

  late final _$_emailAtom =
      Atom(name: '_RegistrationStore._email', context: context);

  @override
  String? get _email {
    _$_emailAtom.reportRead();
    return super._email;
  }

  @override
  set _email(String? value) {
    _$_emailAtom.reportWrite(value, super._email, () {
      super._email = value;
    });
  }

  late final _$_passwordAtom =
      Atom(name: '_RegistrationStore._password', context: context);

  @override
  String? get _password {
    _$_passwordAtom.reportRead();
    return super._password;
  }

  @override
  set _password(String? value) {
    _$_passwordAtom.reportWrite(value, super._password, () {
      super._password = value;
    });
  }

  late final _$_passwordRepeatAtom =
      Atom(name: '_RegistrationStore._passwordRepeat', context: context);

  @override
  String? get _passwordRepeat {
    _$_passwordRepeatAtom.reportRead();
    return super._passwordRepeat;
  }

  @override
  set _passwordRepeat(String? value) {
    _$_passwordRepeatAtom.reportWrite(value, super._passwordRepeat, () {
      super._passwordRepeat = value;
    });
  }

  late final _$_usernameErrorAtom =
      Atom(name: '_RegistrationStore._usernameError', context: context);

  @override
  String? get _usernameError {
    _$_usernameErrorAtom.reportRead();
    return super._usernameError;
  }

  @override
  set _usernameError(String? value) {
    _$_usernameErrorAtom.reportWrite(value, super._usernameError, () {
      super._usernameError = value;
    });
  }

  late final _$_emailErrorAtom =
      Atom(name: '_RegistrationStore._emailError', context: context);

  @override
  String? get _emailError {
    _$_emailErrorAtom.reportRead();
    return super._emailError;
  }

  @override
  set _emailError(String? value) {
    _$_emailErrorAtom.reportWrite(value, super._emailError, () {
      super._emailError = value;
    });
  }

  late final _$_passwordErrorAtom =
      Atom(name: '_RegistrationStore._passwordError', context: context);

  @override
  String? get _passwordError {
    _$_passwordErrorAtom.reportRead();
    return super._passwordError;
  }

  @override
  set _passwordError(String? value) {
    _$_passwordErrorAtom.reportWrite(value, super._passwordError, () {
      super._passwordError = value;
    });
  }

  late final _$_passwordRepeatErrorAtom =
      Atom(name: '_RegistrationStore._passwordRepeatError', context: context);

  @override
  String? get _passwordRepeatError {
    _$_passwordRepeatErrorAtom.reportRead();
    return super._passwordRepeatError;
  }

  @override
  set _passwordRepeatError(String? value) {
    _$_passwordRepeatErrorAtom.reportWrite(value, super._passwordRepeatError,
        () {
      super._passwordRepeatError = value;
    });
  }

  late final _$_activeFormAtom =
      Atom(name: '_RegistrationStore._activeForm', context: context);

  @override
  ActiveForm get _activeForm {
    _$_activeFormAtom.reportRead();
    return super._activeForm;
  }

  @override
  set _activeForm(ActiveForm value) {
    _$_activeFormAtom.reportWrite(value, super._activeForm, () {
      super._activeForm = value;
    });
  }

  late final _$_RegistrationStoreActionController =
      ActionController(name: '_RegistrationStore', context: context);

  @override
  void setActiveForm(ActiveForm activeForm) {
    final _$actionInfo = _$_RegistrationStoreActionController.startAction(
        name: '_RegistrationStore.setActiveForm');
    try {
      return super.setActiveForm(activeForm);
    } finally {
      _$_RegistrationStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setUsername(String? username) {
    final _$actionInfo = _$_RegistrationStoreActionController.startAction(
        name: '_RegistrationStore.setUsername');
    try {
      return super.setUsername(username);
    } finally {
      _$_RegistrationStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setPassword(String? password) {
    final _$actionInfo = _$_RegistrationStoreActionController.startAction(
        name: '_RegistrationStore.setPassword');
    try {
      return super.setPassword(password);
    } finally {
      _$_RegistrationStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setPasswordRepeat(String? passwordRepeat) {
    final _$actionInfo = _$_RegistrationStoreActionController.startAction(
        name: '_RegistrationStore.setPasswordRepeat');
    try {
      return super.setPasswordRepeat(passwordRepeat);
    } finally {
      _$_RegistrationStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setEmail(String? email) {
    final _$actionInfo = _$_RegistrationStoreActionController.startAction(
        name: '_RegistrationStore.setEmail');
    try {
      return super.setEmail(email);
    } finally {
      _$_RegistrationStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void _validateUsername(String? username) {
    final _$actionInfo = _$_RegistrationStoreActionController.startAction(
        name: '_RegistrationStore._validateUsername');
    try {
      return super._validateUsername(username);
    } finally {
      _$_RegistrationStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void _validatePassword(String? password) {
    final _$actionInfo = _$_RegistrationStoreActionController.startAction(
        name: '_RegistrationStore._validatePassword');
    try {
      return super._validatePassword(password);
    } finally {
      _$_RegistrationStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void _validatePasswordRepeat(String? passwordRepeat) {
    final _$actionInfo = _$_RegistrationStoreActionController.startAction(
        name: '_RegistrationStore._validatePasswordRepeat');
    try {
      return super._validatePasswordRepeat(passwordRepeat);
    } finally {
      _$_RegistrationStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void _validateEmail(String? email) {
    final _$actionInfo = _$_RegistrationStoreActionController.startAction(
        name: '_RegistrationStore._validateEmail');
    try {
      return super._validateEmail(email);
    } finally {
      _$_RegistrationStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void validateLogin() {
    final _$actionInfo = _$_RegistrationStoreActionController.startAction(
        name: '_RegistrationStore.validateLogin');
    try {
      return super.validateLogin();
    } finally {
      _$_RegistrationStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void validateRegister() {
    final _$actionInfo = _$_RegistrationStoreActionController.startAction(
        name: '_RegistrationStore.validateRegister');
    try {
      return super.validateRegister();
    } finally {
      _$_RegistrationStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void resetForm() {
    final _$actionInfo = _$_RegistrationStoreActionController.startAction(
        name: '_RegistrationStore.resetForm');
    try {
      return super.resetForm();
    } finally {
      _$_RegistrationStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
activeForm: ${activeForm},
username: ${username},
usernameError: ${usernameError},
password: ${password},
passwordError: ${passwordError},
passwordRepeat: ${passwordRepeat},
passwordRepeatError: ${passwordRepeatError},
email: ${email},
emailError: ${emailError},
hasRegistrationErrors: ${hasRegistrationErrors},
hasLoginErrors: ${hasLoginErrors},
isRegistrationValid: ${isRegistrationValid},
isLoginValid: ${isLoginValid}
    ''';
  }
}
