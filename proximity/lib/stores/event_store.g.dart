// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$EventStore on _EventStore, Store {
  Computed<EventDetails?>? _$eventComputed;

  @override
  EventDetails? get event => (_$eventComputed ??=
          Computed<EventDetails?>(() => super.event, name: '_EventStore.event'))
      .value;
  Computed<ObservableList<Event>>? _$eventsComputed;

  @override
  ObservableList<Event> get events =>
      (_$eventsComputed ??= Computed<ObservableList<Event>>(() => super.events,
              name: '_EventStore.events'))
          .value;
  Computed<bool>? _$isLoadingComputed;

  @override
  bool get isLoading => (_$isLoadingComputed ??=
          Computed<bool>(() => super.isLoading, name: '_EventStore.isLoading'))
      .value;
  Computed<int>? _$pageComputed;

  @override
  int get page => (_$pageComputed ??=
          Computed<int>(() => super.page, name: '_EventStore.page'))
      .value;
  Computed<bool>? _$hasNextPageComputed;

  @override
  bool get hasNextPage =>
      (_$hasNextPageComputed ??= Computed<bool>(() => super.hasNextPage,
              name: '_EventStore.hasNextPage'))
          .value;
  Computed<bool>? _$hasPreviousPageComputed;

  @override
  bool get hasPreviousPage =>
      (_$hasPreviousPageComputed ??= Computed<bool>(() => super.hasPreviousPage,
              name: '_EventStore.hasPreviousPage'))
          .value;

  late final _$_eventAtom = Atom(name: '_EventStore._event', context: context);

  @override
  EventDetails? get _event {
    _$_eventAtom.reportRead();
    return super._event;
  }

  @override
  set _event(EventDetails? value) {
    _$_eventAtom.reportWrite(value, super._event, () {
      super._event = value;
    });
  }

  late final _$_eventsAtom =
      Atom(name: '_EventStore._events', context: context);

  @override
  ObservableList<Event> get _events {
    _$_eventsAtom.reportRead();
    return super._events;
  }

  @override
  set _events(ObservableList<Event> value) {
    _$_eventsAtom.reportWrite(value, super._events, () {
      super._events = value;
    });
  }

  late final _$_loadingAtom =
      Atom(name: '_EventStore._loading', context: context);

  @override
  bool get _loading {
    _$_loadingAtom.reportRead();
    return super._loading;
  }

  @override
  set _loading(bool value) {
    _$_loadingAtom.reportWrite(value, super._loading, () {
      super._loading = value;
    });
  }

  late final _$_pageAtom = Atom(name: '_EventStore._page', context: context);

  @override
  int get _page {
    _$_pageAtom.reportRead();
    return super._page;
  }

  @override
  set _page(int value) {
    _$_pageAtom.reportWrite(value, super._page, () {
      super._page = value;
    });
  }

  late final _$_hasNextPageAtom =
      Atom(name: '_EventStore._hasNextPage', context: context);

  @override
  bool get _hasNextPage {
    _$_hasNextPageAtom.reportRead();
    return super._hasNextPage;
  }

  @override
  set _hasNextPage(bool value) {
    _$_hasNextPageAtom.reportWrite(value, super._hasNextPage, () {
      super._hasNextPage = value;
    });
  }

  late final _$_hasPreviousPageAtom =
      Atom(name: '_EventStore._hasPreviousPage', context: context);

  @override
  bool get _hasPreviousPage {
    _$_hasPreviousPageAtom.reportRead();
    return super._hasPreviousPage;
  }

  @override
  set _hasPreviousPage(bool value) {
    _$_hasPreviousPageAtom.reportWrite(value, super._hasPreviousPage, () {
      super._hasPreviousPage = value;
    });
  }

  late final _$getAsyncAction =
      AsyncAction('_EventStore.get', context: context);

  @override
  Future<void> get() {
    return _$getAsyncAction.run(() => super.get());
  }

  late final _$getByIdAsyncAction =
      AsyncAction('_EventStore.getById', context: context);

  @override
  Future<void> getById(String id) {
    return _$getByIdAsyncAction.run(() => super.getById(id));
  }

  late final _$attendEventAsyncAction =
      AsyncAction('_EventStore.attendEvent', context: context);

  @override
  Future<void> attendEvent(String eventId) {
    return _$attendEventAsyncAction.run(() => super.attendEvent(eventId));
  }

  late final _$cancelAttendEventAsyncAction =
      AsyncAction('_EventStore.cancelAttendEvent', context: context);

  @override
  Future<void> cancelAttendEvent(String eventId) {
    return _$cancelAttendEventAsyncAction
        .run(() => super.cancelAttendEvent(eventId));
  }

  late final _$_EventStoreActionController =
      ActionController(name: '_EventStore', context: context);

  @override
  void setPage(int page) {
    final _$actionInfo =
        _$_EventStoreActionController.startAction(name: '_EventStore.setPage');
    try {
      return super.setPage(page);
    } finally {
      _$_EventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setEvent(EventDetails? event) {
    final _$actionInfo =
        _$_EventStoreActionController.startAction(name: '_EventStore.setEvent');
    try {
      return super.setEvent(event);
    } finally {
      _$_EventStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
event: ${event},
events: ${events},
isLoading: ${isLoading},
page: ${page},
hasNextPage: ${hasNextPage},
hasPreviousPage: ${hasPreviousPage}
    ''';
  }
}
