//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mobx/mobx.dart';
import 'package:proximity/constants/api.dart';
import 'package:proximity/models/user_details.dart';

part 'profile_store.g.dart';

class ProfileStore extends _ProfileStore with _$ProfileStore {}

abstract class _ProfileStore with Store {
  @observable
  UserDetails? _user;

  @observable
  bool _loading = false;

  @computed
  UserDetails? get user {
    return _user;
  }

  @computed
  bool get isLoading {
    return _loading;
  }

  @action
  Future<void> getById(String userId) async {
    _loading = true;

    final http.Client client = http.Client();

    try {
      final http.Response response = await client.get(
        Uri.parse("${ApiPaths.userEndpoint}/$userId"),
      );

      final Map<String, dynamic> json = jsonDecode(response.body);

      _user = UserDetails.fromJson(json);
    } catch (_) {
      _user = null;
    } finally {
      client.close();
      _loading = false;
    }
  }
}
