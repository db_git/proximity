//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:mobx/mobx.dart';

part 'registration_store.g.dart';

const emailRegex =
    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";

enum ActiveForm {
  none,
  login,
  register,
}

class RegistrationStore extends _RegistrationStore with _$RegistrationStore {
  static final RegistrationStore _instance = RegistrationStore._();

  RegistrationStore._();

  factory RegistrationStore() {
    return _instance;
  }
}

abstract class _RegistrationStore with Store {
  @observable
  String? _username;
  @observable
  String? _email;
  @observable
  String? _password;
  @observable
  String? _passwordRepeat;

  @observable
  String? _usernameError;
  @observable
  String? _emailError;
  @observable
  String? _passwordError;
  @observable
  String? _passwordRepeatError;

  @observable
  ActiveForm _activeForm = ActiveForm.none;

  @computed
  ActiveForm get activeForm {
    return _activeForm;
  }

  @action
  void setActiveForm(ActiveForm activeForm) {
    _activeForm = activeForm;
  }

  @action
  void setUsername(String? username) {
    _username = username;
  }

  @computed
  String? get username => _username;

  @computed
  String? get usernameError => _usernameError;

  @action
  void setPassword(String? password) {
    _password = password;
  }

  @computed
  String? get password => _password;

  @computed
  String? get passwordError => _passwordError;

  @action
  void setPasswordRepeat(String? passwordRepeat) {
    _passwordRepeat = passwordRepeat;
  }

  @computed
  String? get passwordRepeat => _passwordRepeat;

  @computed
  String? get passwordRepeatError => _passwordRepeatError;

  @action
  void setEmail(String? email) {
    _email = email;
  }

  @computed
  String? get email => _email;

  @computed
  String? get emailError => _emailError;

  @action
  void _validateUsername(String? username) {
    if (username == null || username.isEmpty) {
      _usernameError = "Username can't be blank.";
      return;
    }

    if (username.contains("@")) {
      _usernameError = "Username can't have '@' sign.";
      return;
    }

    if (username.contains(' ')) {
      _usernameError = "Username can't contain whitespace.";
      return;
    }

    if (username.length < 8) {
      _usernameError = "Username is too short.";
      return;
    }

    if (username.length > 128) {
      _usernameError = "Username is too long.";
      return;
    }

    _usernameError = null;
  }

  @action
  void _validatePassword(String? password) {
    if (password == null || password.isEmpty) {
      _passwordError = "Password can't be empty.";
      return;
    }

    if (password.length < 8) {
      _passwordError = "Password is too short.";
      return;
    }

    _passwordError = null;
  }

  @action
  void _validatePasswordRepeat(String? passwordRepeat) {
    if (passwordRepeat == null ||
        passwordRepeat.isEmpty ||
        passwordRepeat != _password) {
      _passwordRepeatError = "Passwords don't match.";
      return;
    }

    _passwordRepeatError = null;
  }

  @action
  void _validateEmail(String? email) {
    if (email == null || email.isEmpty) {
      _emailError = "E-Mail can't be empty.";
      return;
    }

    if (!RegExp(emailRegex).hasMatch(email)) {
      _emailError = "E-Mail is not valid.";
      return;
    }

    _emailError = null;
  }

  @computed
  bool get hasRegistrationErrors =>
      _usernameError != null || _passwordRepeatError != null || hasLoginErrors;

  @computed
  bool get hasLoginErrors => _emailError != null || _passwordError != null;

  @computed
  bool get isRegistrationValid =>
      _username != null && _passwordRepeat != null && isLoginValid;

  @computed
  bool get isLoginValid => _email != null && _password != null;

  @action
  void validateLogin() {
    _validateEmail(_email);
    _validatePassword(_password);
  }

  @action
  void validateRegister() {
    _validateUsername(_username);
    _validatePasswordRepeat(_passwordRepeat);
    validateLogin();
  }

  @action
  void resetForm() {
    _username = null;
    _email = null;
    _password = null;
    _passwordRepeat = null;

    _usernameError = null;
    _emailError = null;
    _passwordError = null;
    _passwordRepeatError = null;
  }
}
