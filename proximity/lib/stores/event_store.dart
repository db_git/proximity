//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mobx/mobx.dart';
import 'package:proximity/constants/api.dart';
import 'package:proximity/models/event.dart';
import 'package:proximity/models/event_details.dart';

import 'auth_store.dart';

part 'event_store.g.dart';

const int pageSize = 5;

final _authStore = AuthStore();

class EventStore extends _EventStore with _$EventStore {
  static final EventStore _instance = EventStore._();

  EventStore._();

  factory EventStore() {
    return _instance;
  }
}

abstract class _EventStore with Store {
  @observable
  EventDetails? _event;

  @observable
  ObservableList<Event> _events = ObservableList<Event>();

  @observable
  bool _loading = false;

  @observable
  int _page = 1;

  @observable
  bool _hasNextPage = false;

  @observable
  bool _hasPreviousPage = false;

  @computed
  EventDetails? get event => _event;

  @computed
  ObservableList<Event> get events => _events;

  @computed
  bool get isLoading => _loading;

  @computed
  int get page => _page;

  @computed
  bool get hasNextPage => _hasNextPage;

  @computed
  bool get hasPreviousPage => _hasPreviousPage;

  @action
  void setPage(int page) {
    _page = page;
  }

  @action
  void setEvent(EventDetails? event) {
    _event = event;
  }

  @action
  Future<void> get() async {
    _loading = true;
    _events = ObservableList<Event>();

    final http.Client client = http.Client();

    try {
      final http.Response response = await client.get(
        Uri.parse("${ApiPaths.eventEndpoint}?page=$page&size=$pageSize"),
      );

      if (response.statusCode == 200) {
        final Map<String, dynamic> responseBody = jsonDecode(response.body);

        _hasNextPage = responseBody['hasNextPage'];
        _hasPreviousPage = responseBody['hasPreviousPage'];

        final Iterable iterable = responseBody['data'];
        for (final element in iterable) {
          _events.add(Event.fromJson(element));
        }
      }
    } catch (_) {
      _events = ObservableList<Event>();
    } finally {
      client.close();
      _loading = false;
    }
  }

  @action
  Future<void> getById(String id) async {
    _loading = true;

    final http.Client client = http.Client();

    try {
      final http.Response response = await client.get(
        Uri.parse("${ApiPaths.eventEndpoint}/$id"),
      );

      if (response.statusCode == 200) {
        final Map<String, dynamic> responseBody = jsonDecode(response.body);

        _event = EventDetails.fromJson(responseBody);
      }
    } catch (_) {
      _event = null;
    } finally {
      client.close();
      _loading = false;
    }
  }

  @action
  Future<void> attendEvent(String eventId) async {
    final http.Client client = http.Client();

    try {
      await client.post(
        Uri.parse("${ApiPaths.eventAttendEndpoint}/$eventId"),
        headers: {
          "Authorization": "Bearer ${_authStore.jwt}",
        },
      );
    } catch (_) {
      // ignored
    } finally {
      client.close();
    }
  }

  @action
  Future<void> cancelAttendEvent(String eventId) async {
    final http.Client client = http.Client();

    try {
      await client.delete(
        Uri.parse("${ApiPaths.eventAttendEndpoint}/$eventId"),
        headers: {
          "Authorization": "Bearer ${_authStore.jwt}",
        },
      );
    } catch (_) {
      // ignored
    } finally {
      client.close();
    }
  }
}
