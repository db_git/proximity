//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mobx/mobx.dart';
import 'package:proximity/constants/api.dart';
import 'package:proximity/models/event.dart';

part 'event_search_store.g.dart';

class EventSearchStore extends _EventSearchStore with _$EventSearchStore {
  static final EventSearchStore _instance = EventSearchStore._();

  EventSearchStore._();

  factory EventSearchStore() {
    return _instance;
  }
}

abstract class _EventSearchStore with Store {
  @observable
  ObservableList<Event> _events = ObservableList<Event>();

  @observable
  bool _loading = false;

  @observable
  String _search = "";

  @computed
  ObservableList<Event> get events => _events;

  @computed
  bool get isLoading => _loading;

  @computed
  String get search => _search;

  @action
  void setSearch(String search) {
    _search = search;
  }

  @action
  Future<void> searchEvents() async {
    _loading = true;
    _events = ObservableList<Event>();

    final http.Client client = http.Client();

    try {
      if (search.isEmpty) {
        return;
      }

      final String searchEncoded = Uri.encodeFull(search);
      final Uri url = Uri.parse(
          "${ApiPaths.eventEndpoint}?page=1&size=10&search=$searchEncoded");

      final http.Response response = await client.get(url);

      if (response.statusCode == 200) {
        final Map<String, dynamic> responseBody = jsonDecode(response.body);

        final Iterable iterable = responseBody['data'];
        for (final element in iterable) {
          _events.add(Event.fromJson(element));
        }
      }
    } catch (_) {
      _events = ObservableList<Event>();
    } finally {
      client.close();
      _loading = false;
    }
  }
}
