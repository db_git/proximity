// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_search_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$EventSearchStore on _EventSearchStore, Store {
  Computed<ObservableList<Event>>? _$eventsComputed;

  @override
  ObservableList<Event> get events =>
      (_$eventsComputed ??= Computed<ObservableList<Event>>(() => super.events,
              name: '_EventSearchStore.events'))
          .value;
  Computed<bool>? _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading,
              name: '_EventSearchStore.isLoading'))
          .value;
  Computed<String>? _$searchComputed;

  @override
  String get search =>
      (_$searchComputed ??= Computed<String>(() => super.search,
              name: '_EventSearchStore.search'))
          .value;

  late final _$_eventsAtom =
      Atom(name: '_EventSearchStore._events', context: context);

  @override
  ObservableList<Event> get _events {
    _$_eventsAtom.reportRead();
    return super._events;
  }

  @override
  set _events(ObservableList<Event> value) {
    _$_eventsAtom.reportWrite(value, super._events, () {
      super._events = value;
    });
  }

  late final _$_loadingAtom =
      Atom(name: '_EventSearchStore._loading', context: context);

  @override
  bool get _loading {
    _$_loadingAtom.reportRead();
    return super._loading;
  }

  @override
  set _loading(bool value) {
    _$_loadingAtom.reportWrite(value, super._loading, () {
      super._loading = value;
    });
  }

  late final _$_searchAtom =
      Atom(name: '_EventSearchStore._search', context: context);

  @override
  String get _search {
    _$_searchAtom.reportRead();
    return super._search;
  }

  @override
  set _search(String value) {
    _$_searchAtom.reportWrite(value, super._search, () {
      super._search = value;
    });
  }

  late final _$searchEventsAsyncAction =
      AsyncAction('_EventSearchStore.searchEvents', context: context);

  @override
  Future<void> searchEvents() {
    return _$searchEventsAsyncAction.run(() => super.searchEvents());
  }

  late final _$_EventSearchStoreActionController =
      ActionController(name: '_EventSearchStore', context: context);

  @override
  void setSearch(String search) {
    final _$actionInfo = _$_EventSearchStoreActionController.startAction(
        name: '_EventSearchStore.setSearch');
    try {
      return super.setSearch(search);
    } finally {
      _$_EventSearchStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
events: ${events},
isLoading: ${isLoading},
search: ${search}
    ''';
  }
}
