//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

class Event {
  String id;
  String title;
  String? subtitle;
  DateTime date;
  String pictureUrl;
  String location;
  String description;
  int attendees;

  Event({
    required this.id,
    required this.title,
    this.subtitle,
    required this.date,
    required this.pictureUrl,
    required this.location,
    required this.description,
    required this.attendees,
  });

  static Event fromJson(Map<String, dynamic> json) {
    return Event(
      id: json['id'],
      title: json['title'],
      subtitle: json['subtitle'],
      date: DateTime.parse(json['date']).toLocal(),
      pictureUrl: json['pictureUrl'],
      location: json['location'],
      description: json['description'],
      attendees: json['attendees'],
    );
  }

  @override
  String toString() {
    return "Event{"
        "id=$id, "
        "title=$title, "
        "subtitle=$subtitle, "
        "date=$date, "
        "pictureUrl=$pictureUrl, "
        "location=$location, "
        "description=$description, "
        "attendees=$attendees"
        "}";
  }
}
