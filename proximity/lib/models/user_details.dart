//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:proximity/models/event.dart';

class UserDetails {
  String id;
  String username;
  String? avatarUrl;
  List<Event> organizedEvents;
  List<Event> attendedEvents;

  UserDetails({
    required this.id,
    required this.username,
    this.avatarUrl,
    required this.organizedEvents,
    required this.attendedEvents,
  });

  static UserDetails fromJson(Map<String, dynamic> json) {
    final List<Event> organizedEvents = <Event>[];
    final List<Event> attendedEvents = <Event>[];

    for (final element in json['organizedEvents']) {
      organizedEvents.add(Event.fromJson(element));
    }

    for (final element in json['attendedEvents']) {
      attendedEvents.add(Event.fromJson(element));
    }

    return UserDetails(
      id: json['id'],
      username: json['username'],
      avatarUrl: json['pictureUrl'],
      organizedEvents: organizedEvents,
      attendedEvents: attendedEvents,
    );
  }

  @override
  String toString() {
    return "UserDetails{"
        "id=$id, "
        "username=$username, "
        "avatarUrl=$avatarUrl"
        "}";
  }
}
