//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'package:proximity/models/user.dart';

class EventDetails {
  String id;
  String title;
  String? subtitle;
  DateTime date;
  String pictureUrl;
  String location;
  String description;
  int attendeeLimit;
  int price;
  List<User> attendees;
  User organizer;

  EventDetails({
    required this.id,
    required this.title,
    this.subtitle,
    required this.date,
    required this.pictureUrl,
    required this.location,
    required this.description,
    required this.attendeeLimit,
    required this.price,
    required this.attendees,
    required this.organizer,
  });

  static EventDetails fromJson(Map<String, dynamic> json) {
    List<User> attendees = <User>[];

    for (final element in json['attendees']) {
      attendees.add(User.fromJson(element));
    }

    return EventDetails(
      id: json['id'],
      title: json['title'],
      subtitle: json['subtitle'],
      date: DateTime.parse(json['date']).toLocal(),
      pictureUrl: json['pictureUrl'],
      location: json['location'],
      description: json['description'],
      attendeeLimit: json['attendeeLimit'],
      price: json['price'],
      attendees: attendees,
      organizer: User.fromJson(json['organizer']),
    );
  }

  @override
  String toString() {
    return "Event{"
        "id=$id, "
        "title=$title, "
        "subtitle=$subtitle, "
        "date=$date, "
        "pictureUrl=$pictureUrl, "
        "location=$location, "
        "description=$description, "
        "attendeeLimit=$attendeeLimit, "
        "price=$price"
        "}";
  }
}
