//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Proximity.Domain;

public class DatabaseContext : IdentityDbContext<User, IdentityRole<Guid>, Guid>
{
	public DatabaseContext(DbContextOptions options) : base(options) {}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		base.OnModelCreating(builder);

		builder
			.Entity<EventAttendee>()
			.HasKey(ea => new { ea.EventId, ea.UserId });

		builder
			.Entity<EventAttendee>()
			.HasOne(ea => ea.Event)
			.WithMany(e => e.Attendees)
			.HasForeignKey(ea => ea.EventId);

		builder
			.Entity<EventAttendee>()
			.HasOne(ea => ea.User)
			.WithMany(u => u.AttendedEvents)
			.HasForeignKey(ea => ea.UserId);

		builder
			.Entity<Event>()
			.HasOne(e => e.Organizer)
			.WithMany(u => u.OrganizedEvents);
	}

	public DbSet<Event> Events { get; set; } = null!;
}
