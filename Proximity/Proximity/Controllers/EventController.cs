//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.ComponentModel.DataAnnotations;
using System.Net.Mime;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Proximity.Domain;
using Proximity.DTO;
using Proximity.Responses;

namespace Proximity.Controllers;

[ApiController]
[Route("api/events")]
[Produces(MediaTypeNames.Application.Json)]
public class EventController : ControllerBase
{
	private readonly DatabaseContext _context;

	public EventController(DatabaseContext context)
	{
		_context = context;
	}

	[HttpGet]
	[AllowAnonymous]
	public async Task<IActionResult> GetAllAsync(
		[FromQuery] [Range(1, int.MaxValue)] int page,
		[FromQuery] [Range(1, 10)] int size,
		[FromQuery] string? search
	)
	{
		var count = await _context.Events.CountAsync();

		var events = await _context
			.Events
			.Include(e => e.Attendees)
			.ThenInclude(ea => ea.User)
			.OrderByDescending(e => e.Date)
			.ThenByDescending(e => e.Attendees.Count)
			.Skip((page - 1) * size)
			.Take(size)
			.Where(string.IsNullOrEmpty(search)
				? _ => true
				: e => e.Description.ToLower().Contains(search.ToLower())
				|| e.Title.ToLower().Contains(search.ToLower())
				|| e.Location.ToLower().Contains(search.ToLower())
			)
			.AsNoTrackingWithIdentityResolution()
			.ToListAsync();

		if (events.Count is 0) return StatusCode(StatusCodes.Status204NoContent);

		var eventDtos = events
			.Select(e => new EventDto(e))
			.ToList();

		return StatusCode(StatusCodes.Status200OK, new Page<EventDto>(page, size, count, eventDtos));
	}

	[HttpGet]
	[Route("{id:Guid}")]
	[AllowAnonymous]
	public async Task<IActionResult> GetByIdAsync([FromRoute] Guid id)
	{
		var eventEntity = await _context
			.Events
			.Include(e => e.Attendees)
			.ThenInclude(ea => ea.User)
			.Include(e => e.Organizer)
			.Where(e => e.Id == id)
			.AsNoTrackingWithIdentityResolution()
			.FirstOrDefaultAsync();

		if (eventEntity is null) return StatusCode(StatusCodes.Status404NotFound);
		return StatusCode(StatusCodes.Status200OK, new EventDetailsDto(eventEntity));
	}

	[HttpPost]
	[Authorize]
	[Consumes(MediaTypeNames.Application.Json)]
	public async Task<IActionResult> CreateAsync([FromBody] CreateEventDto dto)
	{
		_context.Events.Add(dto.ToEvent());
		await _context.SaveChangesAsync();

		return StatusCode(StatusCodes.Status201Created);
	}

	[HttpDelete]
	[Route("{id:Guid}")]
	[Authorize]
	public async Task<IActionResult> DeleteAsync([FromRoute] Guid id)
	{
		var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
		if (userId is null) return StatusCode(StatusCodes.Status401Unauthorized);

		var isOrganizer = await _context
			.Events
			.Where(e => e.OrganizerId == Guid.Parse(userId) && e.Id == id)
			.CountAsync();

		if (isOrganizer is 0) return StatusCode(StatusCodes.Status418ImATeapot);

		var eventEntity = await _context
			.Events
			.Include(e => e.Attendees)
			.Where(e => e.Id == id)
			.AsTracking()
			.FirstOrDefaultAsync();

		if (eventEntity is null) return StatusCode(StatusCodes.Status404NotFound);

		_context.Entry(eventEntity).State = EntityState.Deleted;
		_context.Events.Remove(eventEntity);
		await _context.SaveChangesAsync();

		return StatusCode(StatusCodes.Status200OK);
	}

	[HttpPost]
	[Route("attend/{id:Guid}")]
	[Authorize]
	public async Task<IActionResult> AttendAsync([FromRoute] Guid id)
	{
		var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
		if (userId is null) return StatusCode(StatusCodes.Status401Unauthorized);

		var isAttending = await _context
			.Events
			.Where(e => e.Attendees.Any(ea => ea.UserId.Equals(userId)))
			.CountAsync();

		if (isAttending is not 0) return StatusCode(StatusCodes.Status400BadRequest);

		var eventEntity = await _context
			.Events
			.Include(e => e.Attendees)
			.Where(e => e.Id == id)
			.AsTracking()
			.FirstOrDefaultAsync();

		if (eventEntity is null) return StatusCode(StatusCodes.Status404NotFound);

		eventEntity.Attendees.Add(new EventAttendee
		{
			UserId = Guid.Parse(userId),
			EventId = eventEntity.Id
		});

		_context.Entry(eventEntity).State = EntityState.Modified;
		await _context.SaveChangesAsync();

		return StatusCode(StatusCodes.Status200OK);
	}

	[HttpDelete]
	[Route("attend/{id:Guid}")]
	[Authorize]
	public async Task<IActionResult> CancelAttendAsync([FromRoute] Guid id)
	{
		var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
		if (userId is null) return StatusCode(StatusCodes.Status401Unauthorized);

		var isAttending = await _context
			.Events
			.Where(e => e.Attendees.Any(ea => ea.UserId.ToString().Equals(userId)))
			.Where(e => e.Attendees.Any(ea => ea.EventId.Equals(id)))
			.CountAsync();

		if (isAttending is 0) return StatusCode(StatusCodes.Status400BadRequest);

		var eventEntity = await _context
			.Events
			.Include(e => e.Attendees)
			.Where(e => e.Id == id)
			.AsTracking()
			.FirstOrDefaultAsync();

		if (eventEntity is null) return StatusCode(StatusCodes.Status404NotFound);

		var attendee = eventEntity
			.Attendees
			.FirstOrDefault(ea => ea.UserId.ToString().Equals(userId) && ea.EventId.Equals(eventEntity.Id));

		if (attendee is null) return StatusCode(StatusCodes.Status400BadRequest);

		eventEntity.Attendees.Remove(attendee);
		_context.Entry(eventEntity).State = EntityState.Modified;
		await _context.SaveChangesAsync();

		return StatusCode(StatusCodes.Status200OK);
	}
}
