//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Net.Mime;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Proximity.Domain;
using Proximity.DTO;
using Proximity.Utils;

namespace Proximity.Controllers;

[ApiController]
[Route("api/users")]
[Produces(MediaTypeNames.Application.Json)]
public class UserController : ControllerBase
{
	private readonly DatabaseContext _context;
	private readonly UserManager<User> _userManager;
	private readonly SignInManager<User> _signInManager;
	private readonly Jwt _jwt;

	public UserController(DatabaseContext context, UserManager<User> userManager, SignInManager<User> signInManager, Jwt jwt)
	{
		_context = context;
		_userManager = userManager;
		_signInManager = signInManager;
		_jwt = jwt;
	}

	[HttpGet]
	[Route("{id:Guid}")]
	[AllowAnonymous]
	public async Task<IActionResult> GetByIdAsync([FromRoute] Guid id)
	{
		var user = await _context
			.Users
			.Where(u => u.Id == id)
			.Include(u => u
				.AttendedEvents
				.Take(10)
				.OrderByDescending(ea => ea.Event.Date)
				.ThenByDescending(ea => ea.Event.Attendees.Count)
			)
			.ThenInclude(ea => ea.Event)
			.ThenInclude(e => e.Attendees)
			.Include(u => u
				.OrganizedEvents
				.Take(10)
				.OrderByDescending(e => e.Date)
				.ThenByDescending(e => e.Attendees.Count)
			)
			.ThenInclude(e => e.Attendees)
			.AsNoTrackingWithIdentityResolution()
			.FirstOrDefaultAsync();

		if (user is null) return StatusCode(StatusCodes.Status404NotFound);
		return StatusCode(StatusCodes.Status200OK, new UserDetailsDto(user));
	}

	[HttpPost]
	[Route("register")]
	[AllowAnonymous]
	[Consumes(MediaTypeNames.Application.Json)]
	public async Task<IActionResult> RegisterAsync([FromBody] UserRegisterDto dto)
	{
		var user = new User
		{
			Email = dto.Email,
			NormalizedEmail = _userManager.NormalizeEmail(dto.Email),
			UserName = dto.Username,
			NormalizedUserName = _userManager.NormalizeName(dto.Username)
		};

		var result = await _userManager.CreateAsync(user, dto.Password);

		if (!result.Succeeded) return StatusCode(StatusCodes.Status400BadRequest);
		return StatusCode(StatusCodes.Status200OK, new { Token = _jwt.CreateToken(user) });
	}

	[HttpPost]
	[Route("login")]
	[AllowAnonymous]
	[Consumes(MediaTypeNames.Application.Json)]
	public async Task<IActionResult> LoginAsync([FromBody] UserLoginDto dto)
	{
		var user = await _userManager.FindByEmailAsync(dto.Email);
		if (user is null) return StatusCode(StatusCodes.Status400BadRequest);

		var result = await _signInManager.PasswordSignInAsync(
			user,
			dto.Password,
			false,
			false
		);

		if (!result.Succeeded) return StatusCode(StatusCodes.Status400BadRequest);
		return StatusCode(StatusCodes.Status200OK, new { Token = _jwt.CreateToken(user) });
	}
}
