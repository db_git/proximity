//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.ComponentModel.DataAnnotations;

namespace Proximity.Validations;

[AttributeUsage(AttributeTargets.Property)]
internal sealed class StringNotEmptyAttribute : ValidationAttribute
{
	public StringNotEmptyAttribute()
	{
		ErrorMessage = "Value can't be empty.";
	}

	protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
	{
		if (value is null) return ValidationResult.Success;

		if (value is not string || (value is string s && string.Empty.Equals(s)))
		{
			return new ValidationResult(ErrorMessage);
		}

		return ValidationResult.Success;
	}
}
