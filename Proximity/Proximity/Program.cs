//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Proximity.Domain;
using Proximity.Utils;

var builder = WebApplication.CreateBuilder(args);

builder.Configuration
	.SetBasePath(AppContext.BaseDirectory)
	.AddJsonFile(builder.Environment.IsDevelopment() ? "appsettings.Development.json" : "appsettings.json")
	.AddEnvironmentVariables();

builder.Services.AddControllers();

builder.Services
	.AddDbContext<DatabaseContext>(options =>
		options.UseNpgsql(builder.Configuration.GetConnectionString("POSTGRESQL"))
			.EnableDetailedErrors()
			.EnableSensitiveDataLogging()
		);

builder.Services
	.AddIdentity<User, IdentityRole<Guid>>(options =>
	{
		options.Password.RequireDigit = false;
		options.Password.RequireLowercase = false;
		options.Password.RequireNonAlphanumeric = false;
		options.Password.RequireUppercase = false;
		options.Password.RequiredLength = 8;

		options.SignIn.RequireConfirmedEmail = false;
		options.SignIn.RequireConfirmedPhoneNumber = false;

		options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_.";
		options.User.RequireUniqueEmail = true;
	})
	.AddEntityFrameworkStores<DatabaseContext>()
	.AddRoles<IdentityRole<Guid>>()
	.AddDefaultTokenProviders().Services
	.Configure<PasswordHasherOptions>(options =>
	{
		options.IterationCount = 16384;
	})
	.Configure<DataProtectionTokenProviderOptions>(options =>
	{
		options.TokenLifespan = TimeSpan.FromHours(24);
	});

var jwtConfig = builder.Configuration.GetSection("Jwt");
builder
	.Services
	.AddAuthentication(opt =>
	{
		opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
		opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
	})
	.AddJwtBearer(opt =>
	{
		opt.SaveToken = true;
		opt.TokenValidationParameters = new TokenValidationParameters
		{
			ValidateIssuer = true,
			ValidateAudience = false,
			ValidateLifetime = true,
			ValidateIssuerSigningKey = true,
			ValidIssuer = jwtConfig.GetSection("Issuer").Value,
			IssuerSigningKey = Jwt.GetSecret(jwtConfig.GetSection("Key").Value!)
		};
	});

builder.Services.AddSingleton<Jwt>();

var app = builder.Build();

app.UseRouting();
app.UseAuthentication();
app.UseRouting();
app.UseAuthorization();
app.MapControllers();

app.Run();
