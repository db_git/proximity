//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.ComponentModel.DataAnnotations;
using Proximity.Domain;
using Proximity.Validations;

namespace Proximity.DTO;

public record CreateEventDto
{
	[Required(AllowEmptyStrings = false)]
	[MinLength(4)]
	[MaxLength(32)]
	public string Title { get; init; } = null!;

	[StringNotEmpty]
	[MinLength(4)]
	[MaxLength(32)]
	public string? Subtitle { get; init; }

	[Required]
	public DateTime? Date { get; init; }

	[Required(AllowEmptyStrings = false)]
	[Url]
	[MinLength(8)]
	[MaxLength(2048)]
	public string PictureUrl { get; init; } = null!;

	[Required(AllowEmptyStrings = false)]
	[MinLength(8)]
	[MaxLength(128)]
	public string Location { get; init; } = null!;

	[Required(AllowEmptyStrings = false)]
	[MinLength(8)]
	[MaxLength(2048)]
	public string Description { get; init; } = null!;

	[Range(0, 1000)]
	public int AttendeeLimit { get; init; }

	[Range(0, 1000)]
	public decimal Price { get; init; }

	[Guid]
	public Guid Organizer { get; init; }

	public Event ToEvent()
	{
		return new Event
		{
			Title = Title,
			Subtitle = Subtitle,
			Date = Date!.Value,
			PictureUrl = PictureUrl,
			Location = Location,
			Description = Description,
			AttendeeLimit = AttendeeLimit,
			Price = Price,
			OrganizerId = Organizer
		};
	}
}
