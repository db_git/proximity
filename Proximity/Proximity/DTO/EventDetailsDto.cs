//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Proximity.Domain;

namespace Proximity.DTO;

public record EventDetailsDto
{
	public Guid Id { get; init; }
	public string Title { get; init; } = null!;
	public string? Subtitle { get; init; }
	public DateTime Date { get; init; }
	public string PictureUrl { get; init; } = null!;
	public string Location { get; init; } = null!;
	public string Description { get; init; } = null!;
	public int AttendeeLimit { get; init; }
	public ICollection<UserDto> Attendees { get; init; } = null!;
	public decimal Price { get; init; }
	public UserDto Organizer { get; init; } = null!;

	public EventDetailsDto() {}

	public EventDetailsDto(Event e)
	{
		Id = e.Id;
		Title = e.Title;
		Subtitle = e.Subtitle;
		Date = e.Date;
		PictureUrl = e.PictureUrl;
		Location = e.Location;
		Description = e.Description;
		AttendeeLimit = e.AttendeeLimit;
		Price = e.Price;
		Attendees = e.Attendees.Select(ea => new UserDto(ea.User)).ToList();
		Organizer = new UserDto(e.Organizer);
	}
}
