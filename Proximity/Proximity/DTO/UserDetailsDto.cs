//    Proximity
//    Copyright (C) 2022  Denis Bošnjaković
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Proximity.Domain;

namespace Proximity.DTO;

public class UserDetailsDto
{
	public Guid Id { get; init; }
	public string Username { get; init; } = null!;
	public string? PictureUrl { get; init; }
	public ICollection<EventDto> OrganizedEvents { get; init; } = null!;
	public ICollection<EventDto> AttendedEvents { get; init; } = null!;

	public UserDetailsDto() {}

	public UserDetailsDto(User u)
	{
		Id = u.Id;
		Username = u.UserName!;
		PictureUrl = u.PictureUrl;
		OrganizedEvents = u.OrganizedEvents.Select(e => new EventDto(e)).ToList();
		AttendedEvents = u.AttendedEvents.Select(ea => new EventDto(ea.Event)).ToList();
	}
}
